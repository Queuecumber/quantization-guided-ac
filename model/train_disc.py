import torch
import torch.nn.functional


def train_disc(batch_idx, autoencoder, model, device, train_loader, optimizer, summary_writer, stats):
    model.train()
    optimizer.zero_grad()

    # Step 1: real batch
    degraded, quantization, original, _ = next(train_loader.__iter__())
    degraded, quantization, original = degraded.to(device), quantization.to(device), original.to(device)

    real_decisions = model(quantization, original)
    labels = torch.full((len(real_decisions),), 1.).to(device)
    real_error = torch.nn.functional.binary_cross_entropy(real_decisions, labels)
    real_error.backward()

    # Step 2: fake batch
    degraded, quantization, original, _ = next(train_loader.__iter__())
    degraded, quantization, original = degraded.to(device), quantization.to(device), original.to(device)

    restored, _, _, _, _ = autoencoder(quantization, degraded)

    restored = restored.detach()
    fake_decisions = model(quantization, restored)
    labels = torch.zeros(len(fake_decisions)).to(device)
    fake_error = torch.nn.functional.binary_cross_entropy(fake_decisions, labels)
    fake_error.backward()

    loss = real_error + fake_error

    optimizer.step()

    if summary_writer is not None:
        summary_writer.add_scalar('loss/loss', global_step=batch_idx, scalar_value=loss)

        if batch_idx % 10 == 0:
            real_accuracy = (real_decisions >= 0.5).float().sum() / len(real_decisions)
            fake_accuracy = (fake_decisions < 0.5).float().sum() / len(fake_decisions)

            summary_writer.add_scalar('metrics/real-accuracy', global_step=batch_idx, scalar_value=real_accuracy)
            summary_writer.add_scalar('metrics/fake-accuracy', global_step=batch_idx, scalar_value=fake_accuracy)

    return loss
