import torch.nn
from .blocknet import BlockNet
from .frequencynet import FrequencyNet
from .weight_init import weight_init
from .frequency_fuser import FrequencyFuser
from .color_restore import ColorRestore
from .spatial_network import SpatialNetwork
from dct import double_nn_dct

from torchvision.utils import save_image


class DeartifactingNetwork(torch.nn.Module):
    def __init__(self, color=False, ablation_settings=None):
        super(DeartifactingNetwork, self).__init__()

        self.color = color

        if ablation_settings is not None:
            self.build(ablation_settings)
        else:
            self.blocks_encode = BlockNet(in_channels=1, out_channels=64, n_layers=1, kernel_size=3, padding=1, activation=torch.nn.PReLU, bias=True)
            self.frequencynet = FrequencyNet(in_channels=64, out_channels=64, activation=torch.nn.PReLU, bias=True)
            self.blocks_decode = BlockNet(in_channels=64, out_channels=64, n_layers=1, kernel_size=3, padding=1, activation=torch.nn.PReLU, bias=True)

            self.fusion = FrequencyFuser(in_channels=[64, 64, 64], fuse_channels=4, kernel_size=3, padding=1, method='perfrequency', activation=torch.nn.PReLU)  

            if self.color:
                self.color_net = ColorRestore(channels=64, n_layers=1, activation=torch.nn.PReLU, bias=True)  

        self.apply(lambda m: weight_init(scale=0.1, m=m))

    def build(self, settings):
        if 'blocks_encode' in settings:
            self.blocks_encode = BlockNet(**settings['blocks_encode'], kernel_size=3, padding=1, activation=torch.nn.PReLU, bias=True)
        else:
            self.blocks_encode = None

        if 'frequencynet' in settings:
            self.frequencynet = FrequencyNet(**settings['frequencynet'], activation=torch.nn.PReLU, bias=True)
        else:
            self.frequencynet = None

        if 'blocks_decode' in settings:
            self.blocks_decode = BlockNet(**settings['blocks_decode'], kernel_size=3, padding=1, activation=torch.nn.PReLU, bias=True)
        else:
            self.blocks_decode = None

        if 'fusion' in settings:
            self.fusion = FrequencyFuser(**settings['fusion'], kernel_size=3, padding=1, method='perfrequency', activation=torch.nn.PReLU)  
        else:
            self.fusion = None


    def __y_net(self, q_y, y):
        if self.blocks_encode is not None:
            blocks_e = self.blocks_encode(q_y, y)
        else:
            blocks_e = y

        if self.frequencynet is not None:
            frequencies = self.frequencynet(blocks_e)
        else:
            frequencies = blocks_e

        if self.blocks_decode is not None:
            blocks_d = self.blocks_decode(q_y, frequencies)
        else:
            blocks_d = frequencies

        if self.fusion is not None:
            y_r = self.fusion(blocks_d, frequencies, blocks_e)
        else:
            y_r = blocks_d

        restored = y + y_r
        return restored

    def forward(self, q_y, y, q_c=None, cb=None, cr=None, y_grad=False):

        if self.color and not y_grad:
            with torch.no_grad():
                restored = self.__y_net(q_y, y)
        else:
            restored = self.__y_net(q_y, y)   

        if self.color:
            r_cb = self.color_net(q_y, q_c, restored, cb)
            r_cr = self.color_net(q_y, q_c, restored, cr)

            restored_cb = double_nn_dct(cb, device=True) + r_cb
            restored_cr = double_nn_dct(cr, device=True) + r_cr

            return restored, restored_cb, restored_cr
        else:
            return restored
