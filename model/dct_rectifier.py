import torch.nn
from dct import blockify, deblockify


class DCTRectifier(torch.nn.Module):
    def __init__(self):
        super(DCTRectifier, self).__init__()

    def forward(self, q, original, restored):
        restored_blocks = blockify(restored, 8)
        original_blocks = blockify(original, 8)

        lower_bounds = restored_blocks < original_blocks - q / 2
        upper_bounds = restored_blocks > original_blocks + q / 2

        restored_blocks[lower_bounds] = (original_blocks - q / 2)[lower_bounds]
        restored_blocks[upper_bounds] = (original_blocks + q / 2)[upper_bounds]

        restored = deblockify(restored_blocks, restored.shape[1], restored.shape[2:]) 

        return restored