import torch
from .coefficient_shuffler import CoefficientShuffler


def safe_concat(x1, x2):
    diffY = x2.shape[2] - x1.shape[2]
    diffX = x2.shape[3] - x1.shape[3]

    x1 = torch.nn.functional.pad(x1, pad=(diffX // 2, diffX - diffX // 2,
                                          diffY // 2, diffY - diffY // 2))

    return torch.cat([x1, x2], 1)


def concat_perfrequency(a, b):
    shuffler = CoefficientShuffler(channels=a.shape[1] // 64)

    a = shuffler.blocks(a, None)
    b = shuffler.blocks(b, None)

    c = safe_concat(a, b)
    
    shuffler._channels = c.shape[1]

    return shuffler.channels(c)