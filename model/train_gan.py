import torch
import tqdm
import torch.nn.functional
from .gan_loss import gan_loss
import dct


def train_gan(batch_idx,
              generator_model,
              discriminator_model,
              device,
              batch,
              g_optimizer,
              d_optimizer,
              minc_model,
              stats,
              summary_writer,
              disc_steps=1,
              gen_steps=1):

    generator_model.train()
    discriminator_model.train()

    # Get batch
    y, cb, cr, q_y, q_c, target, _ = batch
    y, cb, cr, q_y, q_c, target = y.to(device), cb.to(device), cr.to(device), q_y.to(device), q_c.to(device), target.to(device)

    y_r, cb_r, cr_r = generator_model(q_y, y, q_c, cb, cr, y_grad=True)

    gen = torch.cat([y_r, cb_r, cr_r], dim=1)
    gen_spatial = dct.batch_to_images(gen, stats, device)

    compressed = torch.cat([y, dct.double_nn_dct(cb, True), dct.double_nn_dct(cr, True)], dim=1)

    real_spatial = dct.batch_to_images(target, stats, device)
    
    # Train generator
    generator_model.zero_grad()

    decision_real = discriminator_model(target, compressed, q_y, q_c) # For RA gan
    decision_fake = discriminator_model(gen, compressed, q_y, q_c)

    gan_error = gan_loss(gen_spatial,
                            real_spatial,
                            decision_real,
                            decision_fake,
                            minc_model,
                            1e-2,
                            1e-1,
                            5e-4,
                            device)
    gan_error.backward()
    g_optimizer.step()

    if summary_writer is not None:
        summary_writer.add_scalar('loss/generator', global_step=batch_idx, scalar_value=gan_error)

    # Train discriminator
    discriminator_model.zero_grad()

    # Step 1: discriminator real batch
    real_decisions = discriminator_model(target, compressed, q_y, q_c)

    # Step 2: discriminator fake batch
    fake_decisions = discriminator_model(gen.detach(), compressed, q_y, q_c)

    # Backward
    real_error = torch.nn.functional.binary_cross_entropy_with_logits(real_decisions - fake_decisions.mean(), torch.ones_like(real_decisions).to(device)) 

    fake_error = torch.nn.functional.binary_cross_entropy_with_logits(fake_decisions - real_decisions.mean(), torch.zeros_like(fake_decisions).to(device)) 

    ((real_error + fake_error) / 2).backward()
    d_optimizer.step()

    if summary_writer is not None:
        real_accuracy = (torch.sigmoid(real_decisions - fake_decisions.mean()) >= 0.5).float().sum() / len(real_decisions)
        fake_accuracy = (torch.sigmoid(fake_decisions - real_decisions.mean()) < 0.5).float().sum() / len(fake_decisions)
        accuracy = (real_accuracy + fake_accuracy) / 2.

        summary_writer.add_scalar('metrics/real-accuracy', global_step=batch_idx, scalar_value=real_accuracy)
        summary_writer.add_scalar('metrics/fake-accuracy', global_step=batch_idx, scalar_value=fake_accuracy)
        summary_writer.add_scalar('loss/real-loss', global_step=batch_idx, scalar_value=real_error)
        summary_writer.add_scalar('loss/fake-loss', global_step=batch_idx, scalar_value=fake_error)

    