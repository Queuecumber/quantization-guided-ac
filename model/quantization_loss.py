import dct


def quantization_loss(frequency_loss, quantization):
    max_coef_per_batch = quantization.view(-1, 1, 64).max(2)[0].view(-1, 1, 1, 1)
    quantization = quantization / max_coef_per_batch

    frequency_loss = dct.blockify(frequency_loss, 8)
    weighted_loss = frequency_loss * quantization

    return weighted_loss.mean()
