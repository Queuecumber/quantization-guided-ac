from collections import OrderedDict


def merp(model_a, model_b, alpha):
    merped_model = OrderedDict()
    for k in model_a.keys():
        v_a = model_a[k]
        v_b = model_b[k]
        merped_model[k] = alpha * v_a + (1. - alpha) * v_b

    return merped_model
