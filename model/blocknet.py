import torch.nn
import torch
from .convolutional_filter_manifold import ConvolutionalFilterManifold
from .safe_concat import safe_concat
from .rrdb import RRDB


class BlockNet(torch.nn.Module):
    def __init__(self, in_channels, out_channels, n_layers=1, feature_channels=256, kernel_size=1, padding=0, quantization='CMF', activation=torch.nn.LeakyReLU, bias=True):
        super(BlockNet, self).__init__()

        self.quantization = quantization

        if quantization == 'CMF':
            self.block = ConvolutionalFilterManifold(in_channels=in_channels, out_channels=feature_channels, kernel_size=8, stride=8, activation=activation, bias=bias, manifold_bias=True)
        elif quantization == 'concat':
            self.block = torch.nn.Conv2d(in_channels=in_channels + 1, out_channels=feature_channels, kernel_size=8, stride=8, bias=bias)
        elif quantization == 'none':
            self.block = torch.nn.Conv2d(in_channels=in_channels, out_channels=feature_channels, kernel_size=8, stride=8, bias=bias)

        self.block_enhancer = torch.nn.Sequential(*[
            RRDB(channels=feature_channels, kernel_size=kernel_size, padding=padding, activation=activation, bias=bias)
        for _ in range(n_layers)])

        if quantization == 'CMF':
            self.unblock = ConvolutionalFilterManifold(in_channels=feature_channels, out_channels=out_channels, kernel_size=8, stride=8, transposed=True, activation=activation, bias=bias, manifold_bias=True)
        elif quantization == 'concat':
            self.unblock = torch.nn.ConvTranspose2d(in_channels=feature_channels + 64, out_channels=out_channels, kernel_size=8, stride=8, bias=bias)
        elif quantization == 'none':
            self.unblock = torch.nn.ConvTranspose2d(in_channels=feature_channels, out_channels=out_channels, kernel_size=8, stride=8, bias=bias)

        self.a1 = activation()
        self.a2 = activation()


    def forward(self, q, x):

        if self.quantization == 'CMF':
            x1 = self.a1(self.block(q, x))
        elif self.quantization == 'concat':
            qe = q.repeat(1, 1, x.shape[2] // 8, x.shape[3] // 8)
            x1 = self.a1(self.block(torch.cat([x, qe], dim=1)))
        elif self.quantization == 'none':
            x1 = self.a1(self.block(x))

        x2 = self.block_enhancer(x1)
        
        if self.quantization == 'CMF':
            x3 = self.a2(self.unblock(q, x2))
        elif self.quantization == 'concat':
            qe = q.reshape(-1, 64, 1, 1).repeat(1, 1, x2.shape[2], x2.shape[3])
            x3 = self.a2(self.unblock(torch.cat([x2, qe], dim=1)))
        elif self.quantization == 'none':
            x3 = self.a2(self.unblock(x2))
            
        return x3
