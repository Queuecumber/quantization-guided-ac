import torch
from .coefficient_shuffler import CoefficientShuffler


def frequency_task_loss(loss):
    shuffle = CoefficientShuffler(channels=1)
    coefficient_wise = shuffle(loss)

    per_coefficient_loss = torch.split(coefficient_wise, 1, 1)
    per_coefficient_loss = [c.mean() for c in per_coefficient_loss]

    return sum(per_coefficient_loss)
