import torch.nn


class ARCNN(torch.nn.Module):
    def __init__(self):
        super(ARCNN, self).__init__()
        self.net = torch.nn.Sequential(
            torch.nn.Conv2d(1, 64, kernel_size=9, padding=4),
            torch.nn.ReLU(),
            torch.nn.Conv2d(64, 32, kernel_size=7, padding=3),
            torch.nn.ReLU(),
            torch.nn.Conv2d(32, 16, kernel_size=1),
            torch.nn.ReLU(),
            torch.nn.Conv2d(16, 1, kernel_size=5, padding=2)
        )

        self._initialize_weights()

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, torch.nn.Conv2d):
                torch.nn.init.normal_(m.weight, std=0.001)
                torch.nn.init.constant_(m.bias, 0.0)

    def forward(self, x):
        return self.net(x)