import torch.nn
from .coefficient_shuffler import CoefficientShuffler
from .perfrequency_convolution import PerFrequencyConvolution


class FrequencyFuser(torch.nn.Module):
    def __init__(self, in_channels, fuse_channels=64, kernel_size=1, padding=0, method='perfrequency', activation=torch.nn.LeakyReLU, bias=True):
        super(FrequencyFuser, self).__init__()

        total_in = sum(in_channels)

        if method == 'perfrequency':
            self.net = torch.nn.Sequential(
                CoefficientShuffler(channels=total_in, direction='channels'),
                PerFrequencyConvolution(in_channels=total_in, out_channels=fuse_channels, kernel_size=kernel_size, padding=padding, bias=bias),
                activation(),
                PerFrequencyConvolution(in_channels=fuse_channels, out_channels=fuse_channels, kernel_size=kernel_size, padding=padding, bias=bias),
                activation(),
                PerFrequencyConvolution(in_channels=fuse_channels, out_channels=1, kernel_size=kernel_size, padding=padding, bias=bias),
                CoefficientShuffler(channels=1, direction='blocks')
            )
        elif method == 'dilated':
            self.net = torch.nn.Sequential(
                torch.nn.Conv2d(in_channels=total_in, out_channels=fuse_channels, kernel_size=kernel_size, padding=padding * 8, dilation=8, bias=bias),
                activation(),
                torch.nn.Conv2d(in_channels=fuse_channels, out_channels=fuse_channels, kernel_size=kernel_size, padding=padding * 8, dilation=8, bias=bias),
                activation(),
                torch.nn.Conv2d(in_channels=fuse_channels, out_channels=1, kernel_size=kernel_size, padding=padding * 8, dilation=8, bias=bias),
            )
        elif method=='blocks':
            self.net = torch.nn.Sequential(
                torch.nn.Conv2d(in_channels=total_in, out_channels=fuse_channels, kernel_size=8, stride=8, bias=bias),
                activation(),
                torch.nn.Conv2d(in_channels=fuse_channels, out_channels=fuse_channels, kernel_size=kernel_size, padding=padding, bias=bias),
                activation(),
                torch.nn.ConvTranspose2d(in_channels=fuse_channels, out_channels=1, kernel_size=8, stride=8, bias=bias)
            )

    def forward(self, *args):
        x = torch.cat(args, 1)
        x = self.net(x)
        return x
