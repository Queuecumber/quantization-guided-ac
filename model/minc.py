# Adapted from esrgan
# https://github.com/xinntao/BasicSR/blob/master/codes/models/modules/architecture.py
import torch
import torchvision
import torch.nn as nn


class MINCNet(torch.nn.Module):
    def __init__(self):
        super(MINCNet, self).__init__()
        self.ReLU = torch.nn.ReLU(True)
        self.conv11 = torch.nn.Conv2d(3, 64, 3, 1, 1)
        self.conv12 = torch.nn.Conv2d(64, 64, 3, 1, 1)
        self.maxpool1 = torch.nn.MaxPool2d(2, stride=2, padding=0, ceil_mode=True)
        self.conv21 = torch.nn.Conv2d(64, 128, 3, 1, 1)
        self.conv22 = torch.nn.Conv2d(128, 128, 3, 1, 1)
        self.maxpool2 = torch.nn.MaxPool2d(2, stride=2, padding=0, ceil_mode=True)
        self.conv31 = torch.nn.Conv2d(128, 256, 3, 1, 1)
        self.conv32 = torch.nn.Conv2d(256, 256, 3, 1, 1)
        self.conv33 = torch.nn.Conv2d(256, 256, 3, 1, 1)
        self.maxpool3 = torch.nn.MaxPool2d(2, stride=2, padding=0, ceil_mode=True)
        self.conv41 = torch.nn.Conv2d(256, 512, 3, 1, 1)
        self.conv42 = torch.nn.Conv2d(512, 512, 3, 1, 1)
        self.conv43 = torch.nn.Conv2d(512, 512, 3, 1, 1)
        self.maxpool4 = torch.nn.MaxPool2d(2, stride=2, padding=0, ceil_mode=True)
        self.conv51 = torch.nn.Conv2d(512, 512, 3, 1, 1)
        self.conv52 = torch.nn.Conv2d(512, 512, 3, 1, 1)
        self.conv53 = torch.nn.Conv2d(512, 512, 3, 1, 1)

    def forward(self, x):
        out = self.ReLU(self.conv11(x))
        out = self.ReLU(self.conv12(out))
        out = self.maxpool1(out)
        out = self.ReLU(self.conv21(out))
        out = self.ReLU(self.conv22(out))
        out = self.maxpool2(out)
        out = self.ReLU(self.conv31(out))
        out = self.ReLU(self.conv32(out))
        out = self.ReLU(self.conv33(out))
        out = self.maxpool3(out)
        out = self.ReLU(self.conv41(out))
        out = self.ReLU(self.conv42(out))
        out = self.ReLU(self.conv43(out))
        out = self.maxpool4(out)
        out = self.ReLU(self.conv51(out))
        out = self.ReLU(self.conv52(out))
        out = self.conv53(out)
        return out


class MINCFeatureExtractor(torch.nn.Module):
    def __init__(self, path):
        super(MINCFeatureExtractor, self).__init__()

        self.features = MINCNet()
        self.features.load_state_dict(torch.load(path), strict=True)
        self.features.eval()

        for k, v in self.features.named_parameters():
            v.requires_grad = False

    def forward(self, x):
        output = self.features(x)
        return output


class VGGFeatureExtractor(nn.Module):
    def __init__(self, feature_layer=34, use_bn=False, use_input_norm=True,
                 device=torch.device('cpu')):
        super(VGGFeatureExtractor, self).__init__()
        self.use_input_norm = use_input_norm
        if use_bn:
            model = torchvision.models.vgg19_bn(pretrained=True)
        else:
            model = torchvision.models.vgg19(pretrained=True)
        if self.use_input_norm:
            mean = torch.Tensor([0.485, 0.456, 0.406]).view(1, 3, 1, 1).to(device)
            # [0.485 - 1, 0.456 - 1, 0.406 - 1] if input in range [-1, 1]
            std = torch.Tensor([0.229, 0.224, 0.225]).view(1, 3, 1, 1).to(device)
            # [0.229 * 2, 0.224 * 2, 0.225 * 2] if input in range [-1, 1]
            self.register_buffer('mean', mean)
            self.register_buffer('std', std)
        self.features = nn.Sequential(*list(model.features.children())[:(feature_layer + 1)])
        # No need to BP to variable
        for k, v in self.features.named_parameters():
            v.requires_grad = False

    def forward(self, x):
        # Assume input range is [0, 1]
        if self.use_input_norm:
            x = (x - self.mean) / self.std
        output = self.features(x)
        return output