import torch.nn
from .coherent_frequency_block import CoherentFrequencyBlock
from .coefficient_shuffler import CoefficientShuffler


class CoherentFrequencyLayer(torch.nn.Module):
    def __init__(self, n_blocks, in_channels, out_channels, transposed=False, activation=torch.nn.LeakyReLU, bias=True):
        super(CoherentFrequencyLayer, self).__init__()

        layers = []

        if not transposed:
            layers.append(CoherentFrequencyBlock(in_channels, out_channels, transposed, resample=True, activation=activation, bias=bias))
            layer_channels = out_channels
        else:
            layer_channels = in_channels

        for _ in range(n_blocks):
            layers.append(CoherentFrequencyBlock(layer_channels, layer_channels, transposed, resample=False, activation=activation, bias=bias))

        if transposed:
            layers.append(CoherentFrequencyBlock(in_channels, out_channels, transposed, resample=True, activation=activation, bias=bias))

        self.blocks = torch.nn.Sequential(*layers)

    def forward(self, x):
        return self.blocks(x)
