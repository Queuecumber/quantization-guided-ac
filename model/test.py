import torch
from dataset import batch_to_images
from metrics import metrics
import tqdm


def test(batch_idx, model, device, test_loader, summary_writer, stats, color=False):
    model.eval()
    all_psnr = torch.Tensor([0.])
    all_psnrb = torch.Tensor([0.])
    all_ssim = torch.Tensor([0.])
    with tqdm.tqdm(test_loader, desc='Test', unit='images', leave=None) as tt:
        with torch.no_grad():
            for b in tt:
                if color:
                    y, cb, cr, y_q, c_q, target, size = b
                    y, cb, cr, y_q, c_q, target = y.to(device), cb.to(device), cr.to(device), y_q.to(device), c_q.to(device), target.to(device)

                    y_r, cb_r, cr_r = model(y_q, y, c_q, cb, cr)
                    output = torch.cat([y_r, cb_r, cr_r], dim=1)
                else:
                    y, y_q, target, size = b
                    y, y_q, target = y.to(device), y_q.to(device), target.to(device)

                    output = model(y_q, y)

                target_spatial = batch_to_images(target, device=device, crop=size, stats=stats)
                output_spatial = batch_to_images(output, device=device, crop=size, stats=stats)

                psnr, psnrb, ssim = metrics(target_spatial, output_spatial, device=device)

                tt.set_postfix(psnr=psnr.item(), psnrb=psnrb.item(), ssim=ssim.item())

                all_psnr += psnr
                all_psnrb += psnrb
                all_ssim += ssim

        all_psnr = all_psnr / len(test_loader)
        all_psnrb = all_psnrb / len(test_loader)
        all_ssim = all_ssim / len(test_loader)

        if summary_writer is not None:
            summary_writer.add_scalar('test/psnr', global_step=batch_idx, scalar_value=all_psnr)
            summary_writer.add_scalar('test/psnr-b', global_step=batch_idx, scalar_value=all_psnrb)
            summary_writer.add_scalar('test/ssim', global_step=batch_idx, scalar_value=all_ssim)

            summary_writer.add_image('test/result', output_spatial.squeeze(0), global_step=batch_idx, dataformats='CHW')

        tt.disable = False
        tt.set_postfix(psnr=all_psnr.item(), psnrb=all_psnrb.item(), ssim=all_ssim.item(), refresh=True)

    return all_psnr, all_psnrb, all_ssim
