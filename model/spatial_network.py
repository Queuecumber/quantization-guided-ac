import torch.nn
from .rrdb import RRDB
import dct


class SpatialNetwork(torch.nn.Module):
    def __init__(self, in_channels, channels, layers=2):
        super(SpatialNetwork, self).__init__()

        self.in_conv = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels=in_channels,
                            out_channels=channels,
                            kernel_size=3,
                            padding=1,
                            bias=True),
            torch.nn.PReLU(),
        )

        self.net = torch.nn.Sequential(
            *[RRDB(channels=channels, kernel_size=3, padding=1, bias=True) for _ in range(layers)],
        )

        self.out_conv = torch.nn.Sequential(
            torch.nn.Conv2d(in_channels=channels * 2,
                            out_channels=channels,
                            kernel_size=3,
                            padding=1,
                            bias=True),
            torch.nn.PReLU(),
        )

    def forward(self, x, stats):
        x = dct.batch_to_images(x, stats, x.device)

        x = self.in_conv(x)
        xe = self.net(x)
        x = self.out_conv(torch.cat([xe, x], 1))

        x = dct.images_to_batch(x, stats, x.device)

        return x
