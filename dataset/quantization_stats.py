import argparse
import dct
import tqdm
import torch
import tensorboardX
import time
import os


parser = argparse.ArgumentParser()
parser.add_argument('--tag', default='quantization-stats', required=False)
parser.add_argument('--summary_path')
parser.add_argument('--output')
args = parser.parse_args()


qms_luma = []
for q in tqdm.trange(101, desc='Computing Luma Stats'):
    qm = dct.get_coefficients_for_quality(q, table='luma')
    qms_luma.append(qm.view(64))

qms_luma = torch.stack(qms_luma)

mean_luma = qms_luma.mean(0)
var_luma = qms_luma.var(0)
std_luma = torch.sqrt(var_luma)

qms_chroma = []
for q in tqdm.trange(101, desc='Computing Chroma Stats'):
    qm = dct.get_coefficients_for_quality(q, table='chroma')
    qms_chroma.append(qm.view(64))

qms_chroma = torch.stack(qms_chroma)

mean_chroma = qms_chroma.mean(0)
var_chroma = qms_chroma.var(0)
std_chroma = torch.sqrt(var_chroma)

run = time.ctime()
run_path = os.path.join(args.summary_path, args.tag, run)
summary_writer = tensorboardX.SummaryWriter(log_dir=run_path)

for f in tqdm.trange(64, desc='Building Graphs'):
    summary_writer.add_scalars('stats/mean', {
        'luma': mean_luma[f],
        'chroma': mean_chroma[f]
    }, global_step=f)
    summary_writer.add_scalars('stats/var', {
        'luma': var_luma[f],
        'chroma': var_chroma[f]
    }, global_step=f)
    summary_writer.add_scalars('stats/std', {
        'luma': std_luma[f],
        'chroma': std_chroma[f]
    }, global_step=f)
    summary_writer.add_scalars('stats/upperc', {
        'luma': mean_luma[f] + std_luma[f],
        'chroma': mean_chroma[f] + std_chroma[f]
    }, global_step=f)
    summary_writer.add_scalars('stats/lowerc',{
        'luma': mean_luma[f] - std_luma[f],
        'chroma': mean_chroma[f] - std_chroma[f]
    }, global_step=f)

    summary_writer.add_histogram('stats/frequency-luma', values=qms_luma[:, f], global_step=f)
    summary_writer.add_histogram('stats/frequency-chroma', values=qms_chroma[:, f], global_step=f)

summary_writer.add_custom_scalars(layout={
    'composite': {
        'confidence': ['Margin', ['stats/mean', 'stats/lowerc', 'stats/upperc']]
    }
})


torch.save({
    'mean_luma': mean_luma,
    'variance_luma': var_luma,
    'mean_chroma': mean_chroma,
    'variance_chroma': var_chroma
}, args.output)
