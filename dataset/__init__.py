from .jpeg_quantized_dataset import *
from .folder_of_images_dataset import *
from .single_image_dataset import *
from .color_transforms import *
from .spatial_quantized_dataset import *
from .folder_of_jpeg_dataset import *