import argparse
import torch.utils.data
import tqdm
from folder_of_images_dataset import FolderOfImagesDataset
import dct
from torchvision.transforms.functional import to_tensor
import tensorboardX
import os
import time
import random


parser = argparse.ArgumentParser()
parser.add_argument('--images')
parser.add_argument('--summary_path')
parser.add_argument('--tag', required=False, default='dct-stats')
parser.add_argument('--output')
parser.add_argument('--hist_prob', type=float)
parser.add_argument('--batch_size', type=int)
parser.add_argument('--channel', choices=['y', 'cb', 'cr'], required=False, default='y')
parser.add_argument('--merge', required=False)
args = parser.parse_args()

training_datasets = FolderOfImagesDataset(args.images, transform=to_tensor)
training_loader = torch.utils.data.DataLoader(training_datasets, batch_size=args.batch_size, shuffle=True, num_workers=4)

device = torch.device('cuda')

mean = torch.zeros(64).to(device)
var = torch.zeros(64).to(device)
maxs = torch.full(size=(64,), fill_value=-float('inf')).to(device)
mins = torch.full(size=(64,), fill_value=float('inf')).to(device)

blocks_per_batch = (training_datasets[0].shape[1] // 8) * (training_datasets[0].shape[2] // 8) * args.batch_size
hist_index = 0
histogram_data = torch.zeros(int(args.hist_prob * len(training_loader) * blocks_per_batch), 64).to(device)

for i in tqdm.tqdm(training_loader, desc='Computing Statistics'):
    i = i.to(device) * 255.

    if i.shape[1] == 3:
        i = dct.to_ycbcr(i, device)
        c = ['y', 'cb', 'cr'].index(args.channel)
        i = i[:, c:(c+1), :, :]

    i = i - 128
    d = dct.batch_dct(i, device)
    d = dct.blockify(d, 8)
    blocks = d.reshape(-1, 64)
    mean += blocks.mean(0)

    var += blocks.var(0) + blocks.mean(0)**2

    x = torch.max(blocks, 0)[0]
    n = torch.min(blocks, 0)[0]

    maxs = torch.max(maxs, x)
    mins = torch.min(mins, n)

    if random.random() < args.hist_prob and hist_index < len(histogram_data):
        end = min(hist_index + len(blocks), len(histogram_data))
        histogram_data[hist_index:end, :] = blocks[:(end - hist_index), :]
        hist_index = end

mean /= len(training_loader)

var /= len(training_loader)
var -= mean**2

std = torch.sqrt(var)

histogram_data = histogram_data[:hist_index, :]

run = time.ctime()
run_path = os.path.join(args.summary_path, args.tag, run)
summary_writer = tensorboardX.SummaryWriter(log_dir=run_path)

for f in tqdm.trange(64, desc='Building Graphs'):
    summary_writer.add_scalar('stats/mean', mean[f], global_step=f)
    summary_writer.add_scalar('stats/var', var[f], global_step=f)
    summary_writer.add_scalar('stats/std', std[f], global_step=f)
    summary_writer.add_scalar('stats/upperc', mean[f] + std[f], global_step=f)
    summary_writer.add_scalar('stats/lowerc', mean[f] - std[f], global_step=f)
    summary_writer.add_scalar('stats/max', maxs[f], global_step=f)
    summary_writer.add_scalar('stats/min', mins[f], global_step=f)

    summary_writer.add_histogram('stats/frequency', values=histogram_data[:, f], global_step=f)

summary_writer.add_custom_scalars(layout={
    'composite': {
        'confidence': ['Margin', ['stats/mean', 'stats/lowerc', 'stats/upperc']],
        'range': ['Margin', ['stats/mean', 'stats/min', 'stats/max']]
    }
})

if args.merge:
    m = torch.load(args.merge)
    m[args.channel] = {
        'mean': mean.cpu(),
        'variance': var.cpu(),
        'min': mins.cpu(),
        'max': maxs.cpu()
    }

    torch.save(m, args.output)
else:
    torch.save({
            args.channel: {
            'mean': mean.cpu(),
            'variance': var.cpu(),
            'min': mins.cpu(),
            'max': maxs.cpu()
        }
    }, args.output)

summary_writer.close()
