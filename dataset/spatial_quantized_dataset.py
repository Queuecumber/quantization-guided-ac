import torchvision
from dct import *
from .color_transforms import YChannel
from io import BytesIO
from PIL import Image


class SpatialQuantizedDataset(torch.utils.data.Dataset):
    def __init__(self, datasets, qualities, crop=True, grayscale=True, crop_size=100):
        self.datasets = datasets
        self.qualities = qualities

        transform = []

        if crop:
            transform.append(torchvision.transforms.RandomCrop(crop_size))

        transform.append(torchvision.transforms.ToTensor())

        if grayscale:
            transform.append(YChannel())

        self.transform = torchvision.transforms.Compose(transform)

        self.len_images = sum(len(d) for d in self.datasets)

    def denorm(self, im):
        return (im + 1.) / 2.

    def __len__(self):
        return self.len_images * len(self.qualities)

    def __getitem__(self, idx):
        image_index = idx % self.len_images
        quality_index = idx // self.len_images

        for d in self.datasets:
            if image_index >= len(d):
                image_index -= len(d)
            else:
                break

        image = d[image_index]
        image = self.transform(image)

        with BytesIO() as out:
            torchvision.transforms.functional.to_pil_image(image).save(out, format='jpeg', quality=self.qualities[quality_index])
            out.seek(0)
            quantized_image = Image.open(out)
            quantized_image.load()
            quantized_image = torchvision.transforms.functional.to_tensor(quantized_image)

        quantized_image = quantized_image * 2. - 1.
        image = image * 2. - 1.

        return quantized_image, image
