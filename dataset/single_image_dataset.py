import torch
from PIL import Image


class SingleImageDataset(torch.utils.data.Dataset):
    def __init__(self, path):
        self.path = path

        im = Image.open(path)
        im.load()

        self.image = im

    def __len__(self):
        return 1

    def __getitem__(self, idx):
        return self.image
