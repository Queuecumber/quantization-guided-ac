import torch
from .psnr import psnr
from .psnrb import psnrb
from .ssim import ssim


def metrics(target, input, device=None):
    return torch.mean(psnr(target, input), dim=0), torch.mean(psnrb(target, input), dim=0), torch.mean(ssim(target, input, device=device), dim=0)
