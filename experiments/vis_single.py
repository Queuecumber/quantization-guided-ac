import torch
import model
import argparse
import dataset
import metrics
from torchvision.utils import make_grid
import matplotlib.pyplot as plt
import numpy as np
import dct
import time
import os
import tensorboardX

torch.backends.cudnn.benchmark = True


def colormap_image(array):
    cm = plt.get_cmap('plasma')

    if len(array.shape) > 2:
        colored_image = np.zeros([*array.shape, 3])
        for i in range(array.shape[0]):
            colored_image[i] = cm(array[i])[:, :, :3]

        colored_image = torch.Tensor(colored_image).transpose(1, 3)
    else:
        colored_image = cm(array)[:, :, :3]
        colored_image = torch.Tensor(colored_image).transpose(0, 2)

    return colored_image


def save_tensor(tensor, path, summary):
    if tensor.shape[0] > 1:
        tensor = make_grid(tensor.transpose(2, 3), nrow=8, padding=0)
        summary.add_image(tag=path, img_tensor=tensor.squeeze(), dataformats='CWH')
    else:
        summary.add_image(tag=path, img_tensor=tensor.squeeze(), dataformats='HW')


def save_tensor_colormapped(tensor, path, summary):
    c = colormap_image(tensor.squeeze().cpu())

    if len(c.shape) > 3:
        c = make_grid(c, nrow=8, padding=0)
    summary.add_image(tag=path, img_tensor=c, dataformats='CWH')


def compute_metrics(target, input, device, crop, stats):
    target_spatial = dataset.batch_to_images(target, device=device, crop=crop, stats=stats)
    input_spatial = dataset.batch_to_images(input, device=device, crop=crop, stats=stats)

    return metrics.metrics(target_spatial, input_spatial, device=device)


def save_standard(original, degraded, restored, summary, device, crop, stats):
    save_tensor(dataset.batch_to_images(original, device=device, crop=crop, stats=stats), 'spatial/00_original', summary)
    save_tensor(dataset.batch_to_images(degraded, device=device, crop=crop, stats=stats), 'spatial/01_degraded', summary)
    save_tensor(dataset.batch_to_images(restored, device=device, crop=crop, stats=stats), 'spatial/02_restored', summary)
    save_tensor_colormapped(dataset.unprepare_dct(original, stats=stats, device=device), 'frequency/00_original', summary)
    save_tensor_colormapped(dataset.unprepare_dct(degraded, stats=stats, device=device), 'frequency/01_degraded', summary)
    save_tensor_colormapped(dataset.unprepare_dct(restored, stats=stats, device=device), 'frequency/02_restored', summary)


def save_rounding(original, degraded, estimated, summary, device, crop, stats):
    true_rounding = original - degraded
    save_tensor(dataset.batch_to_images(true_rounding, device=device, crop=crop, stats=stats), 'spatial/03_true_rounding', summary)
    save_tensor(dataset.batch_to_images(estimated, device=device, crop=crop, stats=stats), 'spatial/04_estimated_rounding', summary)
    save_tensor_colormapped(dataset.unprepare_dct(true_rounding, stats=stats, device=device), 'frequency/03_true_rounding', summary)
    save_tensor_colormapped(dataset.unprepare_dct(estimated, stats=stats, device=device), 'frequency/04_estimated_rounding', summary)


def save_intermediate(intrablocks_s, interblocks_s, intrablocks_d, interblocks_d, summary, device, crop, stats):
    intrablocks_d = intrablocks_d.squeeze(0).unsqueeze(1)
    intrablocks_s = intrablocks_s.squeeze(0).unsqueeze(1)
    interblocks_s = interblocks_s.squeeze(0).unsqueeze(1)

    save_tensor(dataset.batch_to_images(intrablocks_s, device=device, crop=crop, stats=stats), 'spatial/05_intrablocks_shallow', summary)
    save_tensor(dataset.batch_to_images(interblocks_s, device=device, crop=crop, stats=stats), 'spatial/06_interblocks_shallow', summary)
    save_tensor(dataset.batch_to_images(intrablocks_d, device=device, crop=crop, stats=stats), 'spatial/07_intrablocks_deep', summary)
    save_tensor(dataset.batch_to_images(interblocks_d, device=device, crop=crop, stats=stats), 'spatial/08_interblocks_deep', summary)

    save_tensor_colormapped(dataset.unprepare_dct(intrablocks_s, stats=stats, device=device), 'frequency/05_intrablocks_shallow', summary)
    save_tensor_colormapped(dataset.unprepare_dct(interblocks_s, stats=stats, device=device), 'frequency/06_interblocks', summary)
    save_tensor_colormapped(dataset.unprepare_dct(intrablocks_d, stats=stats, device=device), 'frequency/07_intrablocks_deep', summary)
    save_tensor_colormapped(dataset.unprepare_dct(interblocks_d, stats=stats, device=device), 'frequency/08_interblocks_deep', summary)


parser = argparse.ArgumentParser()
parser.add_argument('--input_image')
parser.add_argument('--checkpoint_path')
parser.add_argument('--summary_path')
parser.add_argument('--tag', required=False, default='vis_single')
parser.add_argument('--stats_path')
parser.add_argument('--quality', type=int)
parser.add_argument('--restart_from', type=int)
parser.add_argument('--save_rounding', action='store_true', required=False, default=False)
parser.add_argument('--save_intermediate', action='store_true', required=False, default=False)
args = parser.parse_args()

device = torch.device('cuda')
stats = dct.DCTStats(args.stats_path)
input_image_dataset = dataset.SingleImageDataset(args.input_image)
quantized_dataset = dataset.JPEGQuantizedDataset([input_image_dataset], [args.quality], crop=False, dct_stats=stats)
loader = torch.utils.data.DataLoader(quantized_dataset, batch_size=1, shuffle=False)

deartifacting_network = model.DeartifactingNetwork().to(device)
model.checkpoint_load(args.checkpoint_path, args.restart_from, deartifacting_network)

run = time.ctime()
run_path = os.path.join(args.summary_path, args.tag, run)
summary_writer = tensorboardX.SummaryWriter(log_dir=run_path)

for dct, quantization, target, size in loader:
    dct, quantization, target = dct.to(device), quantization.to(device), target.to(device)
    with torch.no_grad():
        output, rounding = deartifacting_network(quantization, dct)

input_metrics = compute_metrics(target, dct, device, crop=size, stats=stats)
output_metrics = compute_metrics(target, output, device, crop=size, stats=stats)

print('Input PSNR: {}, PSNR-B: {}, SSIM: {}'.format(*input_metrics))
print('Output PSNR: {}, PSNR-B: {}, SSIM: {}'.format(*output_metrics))

save_standard(target, dct, output, summary_writer, device, crop=size, stats=stats)

if args.save_rounding:
    save_rounding(target, dct, rounding, summary_writer, device, crop=size, stats=stats)

# if args.save_intermediate:
#     save_intermediate(intrablocks_s, interblocks_s, intrablocks_d, interblocks_d, summary_writer, device, crop=size, stats=stats)

summary_writer.close()