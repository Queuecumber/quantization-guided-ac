import torch
import dataset
import model
import argparse
import sys
import tensorboardX
import os
import time
import metrics
from warmup_scheduler import GradualWarmupScheduler


def train(batch_idx, da_model, device, train_loader, optimizer, summary_writer):
    da_model.train()
    quantized, target = next(train_loader.__iter__())
    quantized, target = quantized.to(device), target.to(device)

    optimizer.zero_grad()
    output = da_model(quantized)

    loss = torch.nn.functional.l1_loss(output, target)
    loss.backward()
    optimizer.step()

    print('Train: [{}]\tLoss: {:.6f}'.format(batch_idx, loss.item()))

    if summary_writer is not None:
        summary_writer.add_scalar('loss/l1_loss', global_step=batch_idx, scalar_value=loss)

        if batch_idx % 10 == 0:
            output = training_quantized.denorm(output)
            target = training_quantized.denorm(target)

            summary_writer.add_scalar('metrics/improvement/mse', global_step=batch_idx, scalar_value=model.improvement_loss(output, target, quantized, torch.nn.functional.mse_loss))
            summary_writer.add_scalar('metrics/improvement/l1', global_step=batch_idx, scalar_value=model.improvement_loss(output, target, quantized, torch.nn.functional.l1_loss))

            summary_writer.add_scalar('metrics/psnr', global_step=batch_idx, scalar_value=metrics.psnr(target, output).mean())
            summary_writer.add_scalar('metrics/psnr-b', global_step=batch_idx, scalar_value=metrics.psnrb(target, output).mean())
            summary_writer.add_scalar('metrics/ssim', global_step=batch_idx, scalar_value=metrics.ssim(target, output, device=device).mean())


def test(batch_idx, da_model, device, test_loader, summary_writer):
    da_model.eval()
    all_psnr = []
    all_psnrb = []
    all_ssim = []
    n = 0
    with torch.no_grad():
        for quantized, target in test_loader:
            quantized, target = quantized.to(device), target.to(device)

            output = da_model(quantized)

            output = training_quantized.denorm(output)
            target = training_quantized.denorm(target)

            psnr, psnrb, ssim = metrics.metrics(target, output, device=device)

            all_psnr.append(psnr * len(quantized))
            all_psnrb.append(psnrb * len(quantized))
            all_ssim.append(ssim * len(quantized))
            n += len(quantized)

    all_psnr = sum(all_psnr) / n
    all_psnrb = sum(all_psnrb) / n
    all_ssim = sum(all_ssim) / n

    print('Test: [{}]\tPSNR: {:.6f}, PSNR-B: {:.6f}, SSIM: {:.6f}'.format(batch_idx, all_psnr, all_psnrb, all_ssim))
    summary_writer.add_scalar('test/psnr', global_step=batch_idx, scalar_value=all_psnr)
    summary_writer.add_scalar('test/psnr-b', global_step=batch_idx, scalar_value=all_psnrb)
    summary_writer.add_scalar('test/ssim', global_step=batch_idx, scalar_value=all_ssim)

    return all_psnr, all_psnrb, all_ssim


parser = argparse.ArgumentParser()
parser.add_argument('--training_images', nargs='+')
parser.add_argument('--testing_images', nargs='+')
parser.add_argument('--checkpoint_path')
parser.add_argument('--tag', required=False, default='quantization-independent-train')
parser.add_argument('--summary_path')
parser.add_argument('--learning_rate', type=float, default=1e-4, required=False)
parser.add_argument('--restart_from', type=int, default=None, required=False)
parser.add_argument('--weights_from', type=int, default=None, required=False)
parser.add_argument('--batch_size', type=int)
parser.add_argument('--testing_batch_size', type=int, required=False, default=1)
args = parser.parse_args()

training_datasets = [dataset.FolderOfImagesDataset(p) for p in args.training_images]
testing_datasets = [dataset.FolderOfImagesDataset(p) for p in args.testing_images]

training_qualities = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
testing_qualities = [10]

training_quantized = dataset.SpatialQuantizedDataset(training_datasets, training_qualities, crop=True, grayscale=False)
testing_quantized = dataset.SpatialQuantizedDataset(testing_datasets, testing_qualities, crop=False)

print('{} training examples'.format(len(training_quantized)))
print('{} testing examples'.format(len(testing_quantized)))

training_loader = torch.utils.data.DataLoader(training_quantized, batch_size=args.batch_size, shuffle=True, num_workers=4)
testing_loader = torch.utils.data.DataLoader(testing_quantized, batch_size=args.testing_batch_size, shuffle=False, num_workers=4)

device = torch.device('cuda')
deartifacting_network = model.SpatialMultiscale().to(device)
optimizer = torch.optim.Adam(deartifacting_network.parameters(), lr=args.learning_rate)

scheduler_cosine = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, 100000, eta_min=0)
scheduler_warmup = GradualWarmupScheduler(optimizer, multiplier=100, total_epoch=1000, after_scheduler=scheduler_cosine)

run = time.ctime()

total_params = sum(p.numel() for p in deartifacting_network.parameters() if p.requires_grad)
print('Optimizing {} parameters'.format(total_params))

if args.restart_from is not None:
    run = model.checkpoint_load(args.checkpoint_path, args.restart_from, deartifacting_network, optimizer, scheduler_warmup)
    initial_i = args.restart_from + 1
elif args.weights_from is not None:
    run = model.checkpoint_load(args.checkpoint_path, args.weights_from, deartifacting_network, None, None)
    initial_i = args.weights_from + 1
else:
    initial_i = 0

if torch.cuda.device_count() > 1:
    print('Using {} GPUs'.format(torch.cuda.device_count()))
    deartifacting_network = torch.nn.DataParallel(deartifacting_network)

print('Run at {}'.format(run))
run_path = os.path.join(args.summary_path, args.tag, run)
summary_writer = tensorboardX.SummaryWriter(logdir=run_path)

for i in range(initial_i, sys.maxsize):
    summary_writer.add_scalar('parameters/lr', optimizer.param_groups[0]['lr'], global_step=i)
    train(i, deartifacting_network, device, training_loader, optimizer, summary_writer)

    scheduler_warmup.step()

    if i % 25 == 0:
        psnr, _, _ = test(i, deartifacting_network, device, testing_loader, summary_writer)
        model.checkpoint_save(args.checkpoint_path, i, deartifacting_network, optimizer, scheduler_warmup, run)
