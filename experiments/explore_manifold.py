import argparse
import matplotlib.pyplot as plt
import model
import dct
import torch
import torchvision
from PIL import Image
from pathlib import Path
from MulticoreTSNE import MulticoreTSNE as TSNE
import csv
import numpy as np
import tqdm
import csv
from dct import to_ycbcr

parser = argparse.ArgumentParser()
parser.add_argument('--output', type=Path)
parser.add_argument('--checkpoint_c')
parser.add_argument('--dct_stats')
parser.add_argument('--save_weights', action='store_true')
parser.add_argument('--activate_cmf', action='store_true')
parser.add_argument('--embed_cmf', action='store_true')
args = parser.parse_args()

device = torch.device('cuda')

dct_stats = dct.DCTStats(args.dct_stats)

net = model.DeartifactingNetwork(color=True).to(device)
model.checkpoint_from_file(args.checkpoint_c, net)
net.eval()

def compute_weight(input, cmf):
    return cmf.weight_transform(cmf.manifold(input))


def save_colormapped(weights, output, n=5, nrow=-1):
    if n == -1:
        n = weights.shape[1]

    if nrow == -1:
        nrow = n
    
    w = weights.shape[3]
    h = weights.shape[2]

    weights = weights[:, :n, :, :].reshape(-1, h, w).cpu()
    cm = plt.get_cmap('plasma')
    colored = cm(weights)
    colored = torch.Tensor(colored.transpose(0, 3, 1, 2))[:, :3, :, :] * 255

    grid = torchvision.utils.make_grid(colored, nrow=nrow).numpy().transpose(1, 2, 0)
    Image.fromarray(grid.astype('uint8')).save(output)

if args.save_weights:
    print('Computing weight visualizations')
    visualize_weights = [10, 50, 100]
    mat = torch.stack([dct.get_coefficients_for_quality(q).unsqueeze(0).to(device) for q in visualize_weights])

    with torch.no_grad():
        cey = compute_weight(mat, net.color_net.block_y)

    if args.output_format == 'processed':
        save_colormapped(cey, args.output / 'weight_cey.png')
    else:
        for weights, name in zip([cey], ['cey']):
            w = weights.shape[3]
            h = weights.shape[2]

            for i in range(weights.shape[0]):
                for j in range(0, 32, 15):
                    a = weights[i, j, :, :].reshape(h, w).cpu()
                    cm = plt.get_cmap('plasma')
                    colored = cm(a) * 255

                    grid_store = args.output / 'weights' / name / 'q={}c={}.png'.format(visualize_weights[i],j)
                    grid_store.parent.mkdir(parents=True, exist_ok=True)

                    Image.fromarray(colored.astype('uint8')).save(grid_store)


class SaveFeatures():
    def __init__(self, module):
        self.hook = module.register_forward_hook(self.hook_fn)
    def hook_fn(self, module, input, output):
        self.features = output
    def close(self):
        self.hook.remove()


if args.activate_cmf:
    def max_activation_for_CMF(model, cmf, qualities, filters, gradient_scale):
        activations = SaveFeatures(cmf)
        outputs = []
        for q in tqdm.tqdm(qualities, desc='Qualities', leave=False):
            q_y = dct.get_coefficients_for_quality(q).unsqueeze(0).unsqueeze(0).to(device)
            q_c = dct.get_coefficients_for_quality(q, table='chroma').unsqueeze(0).unsqueeze(0).to(device)

            for f in tqdm.tqdm(filters, desc='Filters', leave=False):
                sz = 32

                img = torch.empty(1, 3, sz, sz).uniform_(0.45, 0.55).to(device)
                img = dct.to_ycbcr(img, device)

                y = img[:, 0:1, :, :]
                cb = torch.nn.functional.interpolate(img[:, 1:2, :, :], scale_factor=0.5, mode='nearest')
                cr = torch.nn.functional.interpolate(img[:, 2:3, :, :], scale_factor=0.5, mode='nearest')

                for r in tqdm.trange(4, desc='Resize steps', leave=False): 
                    if r != 0:
                        y = dct.batch_to_images(yd.detach(), stats=dct_stats, type='y', device=device)
                        cb = dct.batch_to_images(cbd.detach(), stats=dct_stats, type='cb', device=device)
                        cr = dct.batch_to_images(crd.detach(), stats=dct_stats, type='cr', device=device)

                        y = torch.nn.functional.interpolate(y, scale_factor=2, mode='bicubic', align_corners=True)
                        cb = torch.nn.functional.interpolate(cb, scale_factor=2, mode='bicubic', align_corners=True)
                        cr = torch.nn.functional.interpolate(cr, scale_factor=2, mode='bicubic', align_corners=True)

                    yd = dct.images_to_batch(y, stats=dct_stats, type='y', device=device)
                    yd.requires_grad = True
                    cbd = dct.images_to_batch(cb, stats=dct_stats, type='cb', device=device)
                    cbd.requires_grad = True
                    crd = dct.images_to_batch(cr, stats=dct_stats, type='cr', device=device)
                    crd.requires_grad = True

                    optimizer = torch.optim.Adam([yd, cbd, crd], lr=0.1)
                    scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=20, eta_min=1e-5)
                    for n in tqdm.trange(20, desc='Optimizing', leave=False): 
                        optimizer.zero_grad()
                        model(q_y, yd, q_c, cbd, crd, y_grad=True)
                        act = activations.features
                        target_act = torch.mean(act[0, f].reshape(-1))
                        # non_target_act = torch.mean(act[0][[i != f for i in range(act.shape[1])]].reshape(-1))
                        loss = -target_act * gradient_scale
                        tqdm.tqdm.write(str(loss))
                        loss.backward()
                        optimizer.step()
                        scheduler.step()

                y = dct.batch_to_images(yd.detach(), stats=dct_stats, type='y', device=device)
                cb = dct.batch_to_images(cbd.detach(), stats=dct_stats, type='cb', device=device)
                cr = dct.batch_to_images(crd.detach(), stats=dct_stats, type='cr', device=device)

                cb = torch.nn.functional.interpolate(cb, scale_factor=2, mode='bicubic', align_corners=True)
                cr = torch.nn.functional.interpolate(cr, scale_factor=2, mode='bicubic', align_corners=True)

                batch = torch.cat([y, cb, cr], dim=1)
                img = dct.to_rgb(batch, device=device)

                outputs.append(y.squeeze(0))

        activations.close()

        return torch.stack(outputs)

    print('Computing maximal activations w.r.t CMF')
    print('cey')
    img = max_activation_for_CMF(net, net.color_net.block_y, [10, 50, 100], [*range(0, 32, 15)], gradient_scale=1e-12)

    for q in range(3):
        for w in range(3):
            a = img[q * 3 + w, :, :].reshape(img.shape[2], img.shape[3]).cpu().numpy() * 255
            #cm = plt.get_cmap('plasma')
            #colored = cm(a) * 255

            grid_store = args.output / 'vis' / 'cey' / 'q={}c={}.png'.format([10, 50, 100][q],[*range(0, 32, 15)][w])
            grid_store.parent.mkdir(parents=True, exist_ok=True)

            Image.fromarray(a.astype('uint8')).save(grid_store)

if args.embed_cmf:
    print('Computing embeddings')

    inputs_c = torch.stack([dct.get_coefficients_for_quality(q).unsqueeze(0).to(device) for q in range(0, 101, 1)])
    inputs_y = torch.stack([dct.get_coefficients_for_quality(q, table='chroma').unsqueeze(0).to(device) for q in range(0, 101, 1)])
    with torch.no_grad():
        cd = compute_weight(inputs_c, net.color_net.unblock)
        cec = compute_weight(inputs_c, net.blocks_decode.unblock)
        cey = compute_weight(inputs_y, net.blocks_encode.unblock)

    print('cd')
    cd_embeddings = TSNE(n_jobs=4).fit_transform(cd[:, 0:32:15, :, :].reshape(-1, 64).cpu().numpy())
    print('cec')
    cec_embeddings = TSNE(n_jobs=4).fit_transform(cec[:, 0:32:15, :, :].reshape(-1, 64).cpu().numpy())
    print('cey')
    cey_embeddings = TSNE(n_jobs=4).fit_transform(cey[:, 0:32:15, :, :].reshape(-1, 64).cpu().numpy())

    cd_embeddings = cd_embeddings.reshape(101, 3, 2)
    cec_embeddings = cec_embeddings.reshape(101, 3, 2)
    cey_embeddings = cey_embeddings.reshape(101, 3, 2)

    gt_labels = [q in range(10, 101, 10) for q in range(0, 101, 1)]

    c_gt = [q for q in range(0, 101, 1) if gt_labels[q] for _ in range(3)]
    c_n = [q for q in range(0, 101, 1) if not gt_labels[q] for _ in range(3)]

    for embeddings, name in zip([cd_embeddings, cec_embeddings, cey_embeddings], ['cd', 'cec', 'cey']):
        output = args.output / '{}_tsne.csv'.format(name)
        with output.open('w', newline='') as f:
            writer = csv.DictWriter(f, fieldnames=['quality', 'x', 'y', 'gt'])
            writer.writeheader()

            for i in range(embeddings.shape[0]):
                for j in range(embeddings.shape[1]):
                    row = {
                        'quality': i,
                        'x': embeddings[i, j, 0],
                        'y': embeddings[i, j, 1],
                        'gt': gt_labels[i]
                    }

                    writer.writerow(row)