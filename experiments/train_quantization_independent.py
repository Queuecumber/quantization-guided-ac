import torch
import dataset
import model
import argparse
import sys
import tensorboardX
import os
import time
import metrics
from tqdm import tqdm
from itertools import islice


def train(batch_idx, da_model, device, batch, optimizer, summary_writer):
    da_model.train()
    quantized, target = batch
    quantized, target = quantized.to(device), target.to(device)

    optimizer.zero_grad()
    output = da_model(quantized)

    loss = torch.nn.functional.l1_loss(output, target)
    loss.backward()
    optimizer.step()

    if summary_writer is not None:
        summary_writer.add_scalar('loss/l1_loss', global_step=batch_idx, scalar_value=loss)

        if batch_idx % 10 == 0:
            output = training_quantized.denorm(output)
            target = training_quantized.denorm(target)

            summary_writer.add_scalar('metrics/improvement/mse', global_step=batch_idx, scalar_value=model.improvement_loss(output, target, quantized, torch.nn.functional.mse_loss))
            summary_writer.add_scalar('metrics/improvement/l1', global_step=batch_idx, scalar_value=model.improvement_loss(output, target, quantized, torch.nn.functional.l1_loss))

            summary_writer.add_scalar('metrics/psnr', global_step=batch_idx, scalar_value=metrics.psnr(target, output).mean())
            summary_writer.add_scalar('metrics/psnr-b', global_step=batch_idx, scalar_value=metrics.psnrb(target, output).mean())
            summary_writer.add_scalar('metrics/ssim', global_step=batch_idx, scalar_value=metrics.ssim(target, output, device=device).mean())


def test(batch_idx, da_model, device, test_loader, summary_writer):
    da_model.eval()
    all_psnr = []
    all_psnrb = []
    all_ssim = []
    n = 0
    with torch.no_grad():
        for quantized, target in tqdm(test_loader, leave=False):
            quantized, target = quantized.to(device), target.to(device)

            output = da_model(quantized)

            output = training_quantized.denorm(output)
            target = training_quantized.denorm(target)

            psnr, psnrb, ssim = metrics.metrics(target, output, device=device)

            all_psnr.append(psnr * len(quantized))
            all_psnrb.append(psnrb * len(quantized))
            all_ssim.append(ssim * len(quantized))
            n += len(quantized)

    all_psnr = sum(all_psnr) / n
    all_psnrb = sum(all_psnrb) / n
    all_ssim = sum(all_ssim) / n

    summary_writer.add_scalar('test/psnr', global_step=batch_idx, scalar_value=all_psnr)
    summary_writer.add_scalar('test/psnr-b', global_step=batch_idx, scalar_value=all_psnrb)
    summary_writer.add_scalar('test/ssim', global_step=batch_idx, scalar_value=all_ssim)

    return all_psnr, all_psnrb, all_ssim


parser = argparse.ArgumentParser()
parser.add_argument('--training_images', nargs='+')
parser.add_argument('--testing_images', nargs='+')
parser.add_argument('--checkpoint_path')
parser.add_argument('--tag', required=False, default='quantization-independent-train')
parser.add_argument('--summary_path')
parser.add_argument('--learning_rate', type=float, default=1e-4, required=False)
parser.add_argument('--restart_from', type=int, default=None, required=False)
parser.add_argument('--weights_from', type=int, default=None, required=False)
parser.add_argument('--batch_size', type=int)
parser.add_argument('--testing_batch_size', type=int, required=False, default=1)
args = parser.parse_args()

training_datasets = [dataset.FolderOfImagesDataset(p) for p in args.training_images]
testing_datasets = [dataset.FolderOfImagesDataset(p) for p in args.testing_images]

training_qualities = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
testing_qualities = [10]

training_quantized = dataset.SpatialQuantizedDataset(training_datasets, training_qualities, crop=True, grayscale=True)
testing_quantized = dataset.SpatialQuantizedDataset(testing_datasets, testing_qualities, crop=False)

print('{} training examples'.format(len(training_quantized)))
print('{} testing examples'.format(len(testing_quantized)))

training_loader = torch.utils.data.DataLoader(training_quantized, batch_size=args.batch_size, shuffle=True, num_workers=4)
testing_loader = torch.utils.data.DataLoader(testing_quantized, batch_size=args.testing_batch_size, shuffle=False, num_workers=4)

device = torch.device('cuda')
deartifacting_network = model.QuantizationIndependent().to(device)
optimizer = torch.optim.Adam(deartifacting_network.parameters(), lr=args.learning_rate)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=200, gamma=0.5)
run = time.ctime()

total_params = sum(p.numel() for p in deartifacting_network.parameters() if p.requires_grad)
print('Optimizing {} parameters'.format(total_params))

if args.restart_from is not None:
    run = model.checkpoint_load(args.checkpoint_path, args.restart_from, deartifacting_network, optimizer, scheduler)
    initial_i = args.restart_from + 1
    e_0 = initial_i // len(training_loader)
    initial_i = initial_i % len(training_loader) 
elif args.weights_from is not None:
    run = model.checkpoint_load(args.checkpoint_path, args.weights_from, deartifacting_network, None, None)
    initial_i = args.weights_from + 1
    e_0 = initial_i // len(training_loader)
    initial_i = initial_i % len(training_loader) 
else:
    initial_i = 0
    e_0

if torch.cuda.device_count() > 1:
    print('Using {} GPUs'.format(torch.cuda.device_count()))
    deartifacting_network = torch.nn.DataParallel(deartifacting_network)

print('Run at {}'.format(run))
run_path = os.path.join(args.summary_path, args.tag, run)
summary_writer = tensorboardX.SummaryWriter(log_dir=run_path)

for e in range(e_0, sys.maxsize):
    for i, batch in enumerate(islice(tqdm(training_loader, initial=initial_i), initial_i, None), start=initial_i):
        initial_i = 0   
        i = i + e * len(training_loader)
        train(i, deartifacting_network, device, batch, optimizer, summary_writer)

        if i % 25 == 0:
            psnr, _, _ = test(i, deartifacting_network, device, testing_loader, summary_writer)              
            model.checkpoint_save(args.checkpoint_path, i, deartifacting_network, optimizer, scheduler, run)

    scheduler.step()
    summary_writer.add_scalar('parameters/lr', optimizer.param_groups[0]['lr'], global_step=i)
