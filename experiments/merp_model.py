import torch
import model
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--gan_model')
parser.add_argument('--regression_model')
parser.add_argument('--output')
parser.add_argument('--alpha', type=float)
args = parser.parse_args()

ca = torch.load(args.gan_model, map_location='cpu')['model_state']
cb = torch.load(args.regression_model, map_location='cpu')['model_state']

cm = model.merp(ca, cb, args.alpha)

torch.save({
    'model_state': cm
}, args.output)
