import torch
import torch.nn
import dataset
import model
import argparse
import sys
from torch.utils.tensorboard import SummaryWriter
import os
import time
import dct
from tqdm import tqdm
from itertools import islice

parser = argparse.ArgumentParser()
parser.add_argument('--training_images', nargs='+')
parser.add_argument('--testing_images', nargs='+')
parser.add_argument('--checkpoint_path')
parser.add_argument('--tag', required=False, default='jpeg-train')
parser.add_argument('--comment', required=False, default='')
parser.add_argument('--summary_path')
parser.add_argument('--dstats_path', required=False, default='./stats/ystats.pt')
parser.add_argument('--qstats_path', required=False, default='./stats/quantization_stats.pt')
parser.add_argument('--color', action='store_true', required=False, default=False)
parser.add_argument('--grayscale_weights', required=False)
parser.add_argument('--learning_rate', type=float, default=1e-3, required=False)
parser.add_argument('--restart_from', type=int, default=None, required=False)
parser.add_argument('--skip_optim', action='store_true', required=False)
parser.add_argument('--skip_scheduler', action='store_true', required=False)
parser.add_argument('--skip_run', action='store_true', required=False)
parser.add_argument('--batch_size', type=int, required=False, default=32)
parser.add_argument('--testing_batch_size', type=int, required=False, default=1)
parser.add_argument('--loss', choices=['l1', 'quantization', 'frequency', 'ssim', 'error'], required=False, default='error')
parser.add_argument('--single_quality', default=None, required=False, type=int)
args = parser.parse_args()

training_datasets = [dataset.FolderOfImagesDataset(p) for p in args.training_images]
testing_datasets = [dataset.FolderOfImagesDataset(p) for p in args.testing_images]

if not args.single_quality:
    training_qualities = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
    testing_qualities = [10]
else:
    training_qualities = [args.single_quality]
    testing_qualities = [args.single_quality]

dct_stats = dct.DCTStats(args.dstats_path, type='ms')
quantization_stats = dct.QuantizationStats(args.qstats_path, type='01')
training_quantized = dataset.JPEGQuantizedDataset(training_datasets, training_qualities, dct_stats, quantization_stats, crop=False, grayscale=False)
testing_quantized = dataset.JPEGQuantizedDataset(testing_datasets, testing_qualities, dct_stats, quantization_stats, crop=False, grayscale=(not args.color))

print('{} training examples'.format(len(training_quantized)))
print('{} testing examples'.format(len(testing_quantized)))

training_loader = torch.utils.data.DataLoader(training_quantized, batch_size=args.batch_size, shuffle=True, num_workers=4)
testing_loader = torch.utils.data.DataLoader(testing_quantized, batch_size=args.testing_batch_size, shuffle=False, num_workers=4)

device = torch.device('cuda')
deartifacting_network = model.DeartifactingNetwork(color=args.color).to(device)

if args.color:
    optimizer = torch.optim.Adam(deartifacting_network.color_net.parameters(), lr=args.learning_rate)
    model.checkpoint_from_file(args.grayscale_weights, deartifacting_network)
else:
    optimizer = torch.optim.Adam(deartifacting_network.parameters(), lr=args.learning_rate)

#scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='max', verbose=False, patience=10, factor=0.1, threshold_mode='abs')
#scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=100, gamma=0.5)
scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=100, eta_min=1e-6)

run = '{} {}'.format(time.ctime(), args.comment)

if args.restart_from is not None:
    print('Restarting with weights from {} with options:'.format(args.restart_from))
    print('\t[{}] Optimizer'.format(chr(9608) if not args.skip_optim else ' '))
    print('\t[{}] Scheduler'.format(chr(9608) if not args.skip_scheduler else ' '))
    print('\t[{}] Run folder'.format(chr(9608) if not args.skip_run else ' '))
    r = model.checkpoint_load(args.checkpoint_path,
                                args.restart_from,
                                deartifacting_network,
                                optimizer if not args.skip_optim else None,
                                scheduler if not args.skip_scheduler else None)

    if not args.skip_run:
        run = r
        initial_i = args.restart_from + 1
        e_0 = initial_i // len(training_loader)
        initial_i = initial_i % len(training_loader) 
    else:
        e_0 = 0
        initial_i = 0
else:
    e_0 = 0
    initial_i = 0

print('Run at {}'.format(run))
run_path = os.path.join(args.summary_path, args.tag, run)
summary_writer = SummaryWriter(log_dir=run_path, comment=args.comment)

total_params = sum(p.numel() for p in deartifacting_network.parameters() if p.requires_grad)
print('Optimizing {} parameters'.format(total_params))

if torch.cuda.device_count() > 1:
    print('Using {} GPUs'.format(torch.cuda.device_count()))
    deartifacting_network = torch.nn.DataParallel(deartifacting_network)

max_psnr = 0
with tqdm(desc='Completed', initial=(initial_i - 1) // 1000, unit='epoch(s)') as tc:
    with tqdm(total=1000, desc='Epoch', initial=(initial_i - 1) % 1000, unit='batch{{{}}}'.format(args.batch_size)) as te:
        with tqdm(total=25, desc='Train', initial=(initial_i - 1) % 25, unit='batch{{{}}}'.format(args.batch_size)) as tt:
            for epoch in range(e_0, sys.maxsize):
                for i, batch in enumerate(islice(tqdm(training_loader), len(training_loader) - initial_i), start=initial_i):
                    initial_i = 0
                    i = i + epoch * len(training_loader)
                    l = model.train(i, deartifacting_network, device, batch, optimizer, summary_writer, stats=dct_stats, loss_function=args.loss, color=args.color)

                    tt.update()
                    te.update()
                    te.refresh()

                    tt.set_postfix(loss=l.item())

                    summary_writer.add_scalar('parameters/lr', optimizer.param_groups[0]['lr'], global_step=i)
                    te.set_postfix(learning_rate=optimizer.param_groups[0]['lr'])

                    if i % 25 == 0:
                        psnr, _, _ = model.test(i, deartifacting_network, device, testing_loader, summary_writer, stats=dct_stats, color=args.color)

                        if psnr > max_psnr:
                            max_psnr = psnr

                        if i % 1000 == 0:
                            #scheduler.step(metrics=max_psnr)
                            scheduler.step()
                            te.reset()

                            tc.update()

                            if abs(optimizer.param_groups[0]['lr'] - 1e-6) <= sys.float_info.epsilon:
                                break

                        model.checkpoint_save(args.checkpoint_path, i, deartifacting_network, optimizer, scheduler, run)
                        tc.set_postfix(last_checkpoint=i)

                        tt.reset()

                if abs(optimizer.param_groups[0]['lr'] - 1e-6) <= sys.float_info.epsilon:
                    break

tqdm.write('Model converged')
summary_writer.close()
