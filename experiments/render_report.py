from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Float, Integer, String, ForeignKey, Boolean
from sqlalchemy.orm import sessionmaker, relationship
import argparse
import csv
import itertools
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument('--output', type=Path)
parser.add_argument('--type', choices=['ablation', 'comparison'])
parser.add_argument('--comparison_dataset', required=False)
parser.add_argument('--color', action='store_true')
parser.add_argument('--ablation_experiment', required=False)
parser.add_argument('--database')
args = parser.parse_args()

engine = create_engine(args.database)
session = sessionmaker(bind=engine)()
Base = declarative_base()

class AblationExperiment(Base):
    __tablename__ = 'ablation_experiments'

    id = Column(Integer, primary_key=True)
    title = Column(String)

    results = relationship('AblationResult', back_populates='experiment')

class AblationResult(Base):
    __tablename__ = 'ablation_results'

    id = Column(Integer, primary_key=True)
    
    configuration = Column(Integer)
    psnr = Column(Float, default=0)
    psnrb = Column(Float, default=0)
    ssim = Column(Float, default=0)
    experiment_id = Column(Integer, ForeignKey('ablation_experiments.id'))

    experiment = relationship('AblationExperiment', back_populates='results')

class Model(Base):
    __tablename__ = 'models'

    id = Column(Integer, primary_key=True)
    name = Column(String)

    results = relationship('Result', back_populates='model')

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

class Dataset(Base):
    __tablename__ = 'datasets'

    id = Column(Integer, primary_key=True)
    path = Column(String)

    results = relationship('Result', back_populates='dataset')

class Result(Base):
    __tablename__ = 'results'

    id = Column(Integer, primary_key=True)

    psnr = Column(Float, default=0)
    psnrb = Column(Float, default=0)
    ssim = Column(Float, default=0)

    color = Column(Boolean)    
    quality = Column(Integer)

    model_id = Column(Integer, ForeignKey('models.id'))
    dataset_id = Column(Integer, ForeignKey('datasets.id'))

    model = relationship('Model', back_populates='results')
    dataset = relationship('Dataset', back_populates='results')

    def __str__(self):
        return '{psnr:#.2f}/{psnrb:#.2f}/{ssim:#.3f}'.format(**self.__dict__)

    def __repr__(self):
        return '{psnr:#.2f}/{psnrb:#.2f}/{ssim:#.3f}'.format(**self.__dict__)

Base.metadata.create_all(engine)


if args.type == 'comparison':
    result = session.query(Result, Model) \
                .join(Dataset).join(Model) \
                .filter(Dataset.path==args.comparison_dataset, Result.color==args.color) \
                .order_by(Result.quality).all()

    quality_groups = itertools.groupby(result, lambda x: x[0].quality)    
    
    model_order = ['jpeg', 'arcnn', 'mwcnn', 'idcn', 'dmcnn', 'ours']
    args.output.parent.mkdir(parents=True, exist_ok=True)
    with args.output.open('w', newline='') as f:
        writer = csv.DictWriter(f, fieldnames=['Quality', *[m.upper() for m in model_order]])
        writer.writeheader()

        for q, r in quality_groups:
            row = dict(Quality=q, **{m.upper(): '-' for m in model_order})
            for n, m in r:
                row[m.name.upper()] = n
            writer.writerow(row)
elif args.type == 'ablation':
    result = session.query(AblationResult, AblationExperiment) \
                .join(AblationExperiment) \
                .filter(AblationExperiment.title==args.ablation_experiment) \
                .order_by(AblationResult.configuration).all()

    args.output.parent.mkdir(parents=True, exist_ok=True)
    with args.output.open('w', newline='') as f:
        writer = csv.DictWriter(f, fieldnames=['PSNR', 'PSNR-B', 'SSIM'])
        writer.writeheader()
        
        for r, e in result:
            writer.writerow({
                'PSNR': r.psnr,
                'PSNR-B': r.psnrb,
                'SSIM': r.ssim
            })



