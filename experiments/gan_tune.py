import torch
import dataset
import model
import argparse
import sys
import os
import time
import dct
from tqdm import tqdm
from itertools import islice
from torch.utils.tensorboard import SummaryWriter

parser = argparse.ArgumentParser()
parser.add_argument('--training_images', nargs='+')
parser.add_argument('--testing_images', nargs='+')
parser.add_argument('--checkpoint_path')
parser.add_argument('--tag', required=False, default='jpeg-gan')
parser.add_argument('--summary_path')
parser.add_argument('--stats_path')
parser.add_argument('--qstats_path')
parser.add_argument('--g_learning_rate', type=float, default=1e-4, required=False)
parser.add_argument('--d_learning_rate', type=float, default=1e-4, required=False)
parser.add_argument('--restart_from', type=int, default=None, required=False)
parser.add_argument('--skip_optim', action='store_true', required=False)
parser.add_argument('--skip_scheduler', action='store_true', required=False)
parser.add_argument('--skip_run', action='store_true', required=False)
parser.add_argument('--batch_size', type=int)
parser.add_argument('--autoencoder_weights')
parser.add_argument('--gan_iters', type=int, default=1, required=False)
parser.add_argument('--minc_model')
args = parser.parse_args()

training_datasets = [dataset.FolderOfImagesDataset(p) for p in args.training_images]
testing_datasets = [dataset.FolderOfImagesDataset(p) for p in args.testing_images]

training_qualities = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
testing_qualities = [10]

stats = dct.DCTStats(args.stats_path)
qstats = dct.QuantizationStats(args.qstats_path, type='01')
training_quantized = dataset.JPEGQuantizedDataset(training_datasets, training_qualities, stats, qstats, crop=False, grayscale=False)
testing_quantized = dataset.JPEGQuantizedDataset(testing_datasets, testing_qualities, stats, qstats, crop=False, grayscale=False)

print('{} training examples'.format(len(training_quantized)))
print('{} testing examples'.format(len(testing_quantized)))

training_loader = torch.utils.data.DataLoader(training_quantized, batch_size=args.batch_size, shuffle=True, num_workers=4)
testing_loader = torch.utils.data.DataLoader(testing_quantized, batch_size=1, shuffle=False, num_workers=4)

device = torch.device('cuda')

generator = model.DeartifactingNetwork(color=True).to(device)
g_optimizer = torch.optim.Adam(generator.parameters(), lr=args.g_learning_rate)
#g_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(g_optimizer, T_max=3 * len(training_loader), eta_min=1e-6)

discriminator = model.Discriminator().to(device)
d_optimizer = torch.optim.Adam(discriminator.parameters(), lr=args.d_learning_rate)
#d_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(d_optimizer, T_max=6 * len(training_loader), eta_min=1e-6)

minc_model = model.MINCFeatureExtractor(args.minc_model).to(device)
#minc_model = model.VGGFeatureExtractor(feature_layer=34, use_bn=False, device=device).to(device)

run = time.ctime()

if args.restart_from is not None:
    print('Restarting with weights from {} with options:'.format(args.restart_from))
    print('\t[{}] Optimizer'.format(chr(9608) if not args.skip_optim else ' '))
    print('\t[{}] Scheduler'.format(chr(9608) if not args.skip_scheduler else ' '))
    print('\t[{}] Run folder'.format(chr(9608) if not args.skip_run else ' '))
    r = model.checkpoint_load(args.checkpoint_path,
                              args.restart_from,
                              generator,
                              g_optimizer if not args.skip_optim else None,
                              None,
                              discriminator,
                              d_optimizer if not args.skip_optim else None,
                              None)

    if not args.skip_run:
        run = r
        initial_i = args.restart_from + 1
        initial_e = initial_i // len(training_loader)
        initial_i = initial_i % len(training_loader)
    else:
        initial_i = 0
        initial_e = 0
else:
    if args.autoencoder_weights is None:
        print('Attempting fresh start with no autoencoder')
        exit(-1)
    else:
        print('Loading autoencoder from {}'.format(args.autoencoder_weights))
        model.checkpoint_from_file(args.autoencoder_weights, generator)

    initial_i = 0
    initial_e = 0

if torch.cuda.device_count() > 1:
    print('Using {} GPUs'.format(torch.cuda.device_count()))
    generator = torch.nn.DataParallel(generator)
    discriminator = torch.nn.DataParallel(discriminator)
    minc_model = torch.nn.DataParallel(minc_model)

print('Run at {}'.format(run))
run_path = os.path.join(args.summary_path, args.tag, run)
summary_writer = SummaryWriter(log_dir=run_path)

for epoch in range(initial_e, 3):
    with tqdm(training_loader, desc='Train', unit='batch{{{}}}'.format(32), initial=initial_i-1) as tt:
        for i, batch in enumerate(islice(tt, len(training_loader) - initial_i), start=initial_i):
            initial_i = 0
            i = i + epoch * len(training_loader)
            model.train_gan(i,
                            generator,
                            discriminator,
                            device,
                            batch,
                            g_optimizer,
                            d_optimizer,
                            minc_model,
                            stats,
                            summary_writer,
                            args.gan_iters, args.gan_iters)

            #g_scheduler.step()
            #d_scheduler.step()

            summary_writer.add_scalar('parameters/g_lr', g_optimizer.param_groups[0]['lr'], global_step=i)
            summary_writer.add_scalar('parameters/d_lr', d_optimizer.param_groups[0]['lr'], global_step=i)

            tt.set_postfix(g_rate=g_optimizer.param_groups[0]['lr'],
                           d_rate=d_optimizer.param_groups[0]['lr'])

            if i % 25 == 0:
                psnr, _, _ = model.test(i, generator, device, testing_loader, summary_writer, stats=stats, color=True)

                model.checkpoint_save(args.checkpoint_path,
                                        i,
                                        generator,
                                        g_optimizer,
                                        None,
                                        run,
                                        discriminator,
                                        d_optimizer,
                                        None)

                tt.set_postfix(last_checkpoint=i)

