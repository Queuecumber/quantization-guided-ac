import torch
import argparse
import dct
import metrics
import dataset
from torchvision.transforms.functional import to_pil_image
from PIL import Image
from skimage.color import rgb2ycbcr
import numpy as np
import torchjpeg.codec
import model
from dct import deblockify
from pathlib import Path


def dequantize_channel(channel, quantization):
        dequantized_dct = channel.float() * quantization
        dequantized_dct = dequantized_dct.view(1, dequantized_dct.shape[1] * dequantized_dct.shape[2], 8, 8)
        dequantized_dct = dct.deblockify(dequantized_dct, 1, (channel.shape[1] * 8, channel.shape[2] * 8))

        return dequantized_dct

def pad_to(channel, size):
    s = torch.Tensor(list(channel.shape))[2:]
    add_blocks = (torch.ceil(s / size) * size - s).long()

    channel = torch.nn.functional.pad(channel, [0, add_blocks[1], 0, add_blocks[0]])
    return channel

def extract_overlapping_blocks(channel, size):
    b = torch.nn.functional.unfold(channel, kernel_size=size, stride=size // 2, padding=size // 4)
    b = b.transpose(1, 2)
    b = b.view(-1, 1, size, size)
    return b

parser = argparse.ArgumentParser()
parser.add_argument('--input')
parser.add_argument('--output', type=Path)
parser.add_argument('--dstats_path')
parser.add_argument('--color', action='store_true')
parser.add_argument('--weights')
parser.add_argument('--merp_from', required=False)
parser.add_argument('--merp_alpha', type=float, required=False)
parser.add_argument('--quality_override', type=int, required=False, default=None)
parser.add_argument('--output_tensor', action='store_true')
args = parser.parse_args()

device = torch.device('cuda')
net = model.DeartifactingNetwork(color=args.color).to(device)

if args.merp_from is not None:
    model.checkpoint_merp(args.weights, args.merp_from, args.merp_alpha, net)
else:
    model.checkpoint_from_file(args.weights, net)

net.eval()

stats = dct.DCTStats(args.dstats_path, type='ms')
qstats = dct.QuantizationStats(None, type='01')

dim, quantization, y, cbcr = torchjpeg.codec.read_coefficients(args.input)
q_y = quantization[0].float()


y = dequantize_channel(y, q_y).to(device)
y = dct.prepare_dct(y, stats, device=device, type='y')

if args.quality_override is not None:
    q_y = dct.get_coefficients_for_quality(args.quality_override)

q_y = qstats.forward(q_y.unsqueeze(0).unsqueeze(0)).to(device)

with torch.no_grad():
    if args.color:
        q_c = quantization[1].float()

        cb = cbcr[0].unsqueeze(0)
        cr = cbcr[1].unsqueeze(0)

        cb = dequantize_channel(cb, q_c)
        cr = dequantize_channel(cr, q_c)

        if cb.shape[2] == y.shape[2]:
            cb = dct.half_nn_dct(cb)
            cr = dct.half_nn_dct(cr)

        cb = dct.prepare_dct(cb, stats, type='cb').to(device)
        cr = dct.prepare_dct(cr, stats, type='cr').to(device)

        if args.quality_override is not None:
            q_c  = dct.get_coefficients_for_quality(args.quality_override, table='chroma')

        q_c = qstats.forward(q_c.unsqueeze(0).unsqueeze(0)).to(device)

        if y.shape[2] != cb.shape[2] * 2:
            y = torch.nn.functional.pad(y, [0, 0, 0, 8])

        if y.shape[3] != cb.shape[3] * 2:
            y = torch.nn.functional.pad(y, [0, 8, 0, 0])

        if y.shape[2] > 2000 or y.shape[3] > 2000:
            y = pad_to(y, 256)
            cb = pad_to(cb, 128)
            cr = pad_to(cr, 128)

            y_r = []
            cb_r = []
            cr_r = []

            y_blocks = extract_overlapping_blocks(y, 512)
            cb_blocks = extract_overlapping_blocks(cb, 256)
            cr_blocks = extract_overlapping_blocks(cr, 256)

            for b in range(y_blocks.shape[0]):
                is_y = y_blocks[b].unsqueeze(0)
                is_cb = cb_blocks[b].unsqueeze(0)
                is_cr = cr_blocks[b].unsqueeze(0)

                os_y, os_cb, os_cr = net(q_y, is_y, q_c, is_cb, is_cr)

                y_r.append(os_y[:, :, 128:384, 128:384].reshape(1, 1, 256, 256))
                cb_r.append(os_cb[:, :, 128:384, 128:384].reshape(1, 1, 256, 256))
                cr_r.append(os_cr[:, :, 128:384, 128:384].reshape(1, 1, 256, 256))

            y_r = deblockify(torch.cat(y_r, dim=1), 1, (y.shape[2], y.shape[3]))
            cb_r = deblockify(torch.cat(cb_r, dim=1), 1, (y.shape[2], y.shape[3]))
            cr_r = deblockify(torch.cat(cr_r, dim=1), 1, (y.shape[2], y.shape[3]))
        else:
            y_r, cb_r, cr_r = net(q_y, y, q_c, cb, cr)
        output = torch.cat([y_r, cb_r, cr_r], dim=1)
    else:
        if y.shape[2] > 2000 or y.shape[3] > 2000:
            y = pad_to(y, 256)
            output = []

            y_blocks = extract_overlapping_blocks(y, 512)

            for b in range(y_blocks.shape[0]):
                is_y = y_blocks[b].unsqueeze(0)
                
                os_y = net(q_y, is_y)

                output.append(os_y[:, :, 128:384, 128:384].reshape(1, 1, 256, 256))

            output = deblockify(torch.cat(output, dim=1), 1, (y.shape[2], y.shape[3]))
        else:
            output = net(q_y, y)

output_spatial = dct.batch_to_images(output, device=device, crop=dim[0], stats=stats)


to_pil_image(output_spatial.squeeze().cpu()).save(args.output)

if args.output_tensor:
    torch.save(output_spatial.squeeze().cpu(), str(args.output.with_suffix('.pt')))