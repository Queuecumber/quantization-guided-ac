import torch
import dataset
import model
import argparse
import sys
from torch.utils.tensorboard import SummaryWriter
import os
import time
import metrics
from tqdm import tqdm
from itertools import islice


def train(batch_idx, da_model, device, batch, optimizer, summary_writer):
    da_model.train()
    quantized, target = batch
    quantized, target = quantized.to(device), target.to(device)

    optimizer.zero_grad()
    output = da_model(quantized)

    loss = torch.nn.functional.mse_loss(output, target)
    loss.backward()
    optimizer.step()

    if summary_writer is not None:
        summary_writer.add_scalar('loss/l2_loss', global_step=batch_idx, scalar_value=loss)


def test(batch_idx, da_model, device, test_loader, summary_writer):
    da_model.eval()
    all_psnr = []
    all_psnrb = []
    all_ssim = []
    n = 0
    with torch.no_grad():
        for quantized, target in tqdm(test_loader, leave=False):
            quantized, target = quantized.to(device), target.to(device)

            output = da_model(quantized)

            output = training_quantized.denorm(output)
            target = training_quantized.denorm(target)

            psnr, psnrb, ssim = metrics.metrics(target, output, device=device)

            all_psnr.append(psnr * len(quantized))
            all_psnrb.append(psnrb * len(quantized))
            all_ssim.append(ssim * len(quantized))
            n += len(quantized)

    all_psnr = sum(all_psnr) / n
    all_psnrb = sum(all_psnrb) / n
    all_ssim = sum(all_ssim) / n

    summary_writer.add_scalar('test/psnr', global_step=batch_idx, scalar_value=all_psnr)
    summary_writer.add_scalar('test/psnr-b', global_step=batch_idx, scalar_value=all_psnrb)
    summary_writer.add_scalar('test/ssim', global_step=batch_idx, scalar_value=all_ssim)

    return all_psnr, all_psnrb, all_ssim


parser = argparse.ArgumentParser()
parser.add_argument('--training_images', nargs='+')
parser.add_argument('--testing_images', nargs='+')
parser.add_argument('--checkpoint_path')
parser.add_argument('--tag', required=False, default='arcnn-train')
parser.add_argument('--summary_path')
parser.add_argument('--restart_from', type=int, default=None, required=False)
parser.add_argument('--weights_from', type=int, default=None, required=False)
args = parser.parse_args()

training_datasets = [dataset.FolderOfImagesDataset(p) for p in args.training_images]
testing_datasets = [dataset.FolderOfImagesDataset(p) for p in args.testing_images]

training_quantized = dataset.SpatialQuantizedDataset(training_datasets, [20], crop=True, grayscale=True, crop_size=32)
testing_quantized = dataset.SpatialQuantizedDataset(testing_datasets, [10], crop=False)

print('{} training examples'.format(len(training_quantized)))
print('{} testing examples'.format(len(testing_quantized)))

training_loader = torch.utils.data.DataLoader(training_quantized, batch_size=128, shuffle=True, num_workers=4)
testing_loader = torch.utils.data.DataLoader(testing_quantized, batch_size=1, shuffle=False, num_workers=4)

device = torch.device('cuda')
deartifacting_network = model.ARCNN().to(device)
optimizer = torch.optim.SGD(deartifacting_network.parameters(), lr=1e-4, momentum=0.9)
run = time.ctime()

total_params = sum(p.numel() for p in deartifacting_network.parameters() if p.requires_grad)
print('Optimizing {} parameters'.format(total_params))

if args.restart_from is not None:
    run = model.checkpoint_load(args.checkpoint_path, args.restart_from, deartifacting_network, optimizer)
    initial_i = args.restart_from + 1
    e_0 = initial_i // len(training_loader)
    initial_i = initial_i % len(training_loader) 
elif args.weights_from is not None:
    run = model.checkpoint_load(args.checkpoint_path, args.weights_from, deartifacting_network, None, None)
    initial_i = args.weights_from + 1
    e_0 = initial_i // len(training_loader)
    initial_i = initial_i % len(training_loader) 
else:
    initial_i = 0
    e_0 = 0

if torch.cuda.device_count() > 1:
    print('Using {} GPUs'.format(torch.cuda.device_count()))
    deartifacting_network = torch.nn.DataParallel(deartifacting_network)

print('Run at {}'.format(run))
run_path = os.path.join(args.summary_path, args.tag, run)
summary_writer = SummaryWriter(log_dir=run_path)

print('Phase 1: Quality 20 training ')
done = False
for e in range(e_0, sys.maxsize):
    for i, batch in enumerate(islice(tqdm(training_loader, initial=initial_i), initial_i, None), start=initial_i):
        initial_i = 0   
        i = i + e * len(training_loader)
        train(i, deartifacting_network, device, batch, optimizer, summary_writer)

        if i % 1000 == 0:
            psnr, _, _ = test(i, deartifacting_network, device, testing_loader, summary_writer)              
            model.checkpoint_save(args.checkpoint_path, i, deartifacting_network, optimizer, None, run)

        if i >= 2500000:
            done = True
            break

    if done:
        break

print('Phase 2: Transfre to quality 10 training ')
training_quantized = dataset.SpatialQuantizedDataset(training_datasets, [10], crop=True, grayscale=True, crop_size=32)
training_loader = torch.utils.data.DataLoader(training_quantized, batch_size=128, shuffle=True, num_workers=4)
done = False
for e in range(e_0, sys.maxsize):
    for i, batch in enumerate(islice(tqdm(training_loader, initial=initial_i), initial_i, None), start=initial_i):
        initial_i = 0   
        i = i + e * len(training_loader)
        train(i, deartifacting_network, device, batch, optimizer, summary_writer)

        if i % 1000 == 0:
            psnr, _, _ = test(i, deartifacting_network, device, testing_loader, summary_writer)              
            model.checkpoint_save(args.checkpoint_path, i, deartifacting_network, optimizer, None, run)

        if i >= 2500000:
            done = True
            break

    if done:
        break

print('Model Converged')
summary_writer.close()
