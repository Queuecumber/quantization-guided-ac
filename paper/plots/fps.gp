#!/usr/bin/gnuplot -c

set terminal pdf enhanced color font "Helvetica, 30" size 6, 5

set key autotitle columnhead 

unset key
unset colorbox

load 'plasma.pal'

set grid ytics xtics

set ylabel 'Increase in SSIM'
set xlabel 'FPS'

set xtics 1
set ytics 0.005

load 'plasma.pal'

set datafile separator ','

set output "fps.pdf"

set cbrange [0:3]
set xrange[0:6]
set yrange[0.045:0.07]
plot "../data/runtime.csv" every ::::2 using 2:3:1:0 with labels left point linestyle 1 pointtype 7 pointsize 2 palette offset char 1, 0 font "Helvetica, 18", \
     "../data/runtime.csv" every ::3::4 using 2:3:1:($0 + 3) with labels left point linestyle 1 pointtype 7 pointsize 2 palette offset char 1, 0 font "Helvetica Bold, 18", \
     "../data/runtime.csv" every ::3::4 using 2:3 with point pointtype 6 pointsize 2 linewidth 3 linecolor 'black'


