#!/usr/bin/gnuplot -c

set terminal pdf enhanced color font "Helvetica, 30" size 7, 5

unset key

set grid y

unset colorbox

set style fill solid

set xlabel 'Input Quality'

set style data histogram
set style histogram cluster gap 1
set boxwidth 0.9

set ytics 20 
set xtics scale 0

set xrange [0.4:5.6]

set datafile separator ','

load 'plasma.pal'

set output "equivalent_quality.pdf"
set ylabel 'Equivalent Quality'
plot "../data/equivalent_quality.csv" using ($2 + $1):xticlabels(sprintf('%d', $1)) linestyle 3 title 'Equivalent Quality'

set ytics 2
set ylabel 'Space Saved (kB)'
set output "space_saving.pdf"
plot "../data/equivalent_quality.csv" using ($3 / 1000):xticlabels(sprintf('%d', $1)) linestyle 6 title 'Space Savings'

