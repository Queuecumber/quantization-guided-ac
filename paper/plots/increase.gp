#!/usr/bin/gnuplot -c

set terminal pdf enhanced color font "Helvetica, 30" size 7, 5

set key vertical right maxrows 3 samplen 1

set grid ytics

unset colorbox

set style fill solid

set ylabel 'Increase in PSNR (dB)'
set xlabel 'Quality'

set style data histogram
set style histogram cluster gap 1
set boxwidth 0.9

set yrange [0:3]
set xrange [0:11]

set ytics 1
set xtics scale 0

set output "increase.pdf"

format = '"%lf,%lf/%*lf/%*lf,-,-,-,-,%lf/%*lf/%*lf"'

load 'plasma.pal'

plot "../data/increase/live1_increase.csv" using ($3 - $2):xticlabels(sprintf('%d', $1)) @format title 'Live-1' linestyle 1, \
     "../data/increase/BSDS500_increase.csv" using ($3 - $2) @format title ' BSDS500' linestyle 5, \
     "../data/increase/ICB-RGB8_increase.csv" using ($3 - $2) @format title 'ICB' linestyle 8

