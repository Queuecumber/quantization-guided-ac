#!/usr/bin/gnuplot -c

set terminal pdf enhanced color font "Helvetica, 30" size 7.5, 5

set key autotitle columnhead samplen 1 vertical maxrows 2 above

set grid ytics

unset colorbox

set style fill solid

set ylabel 'Probability'
set xlabel 'Frequency'

set style data histogram
set style histogram cluster gap 1
set boxwidth 0.9

set yrange [0:1.05]
set xrange [-1:15]

set ytics 0.2
set xtics scale 0

load 'plasma.pal'

set datafile separator ','

set output "frequencies_woman.pdf"
plot "../data/frequencies/woman.csv" using 2:xticlabels(sprintf('%d', $1)) linestyle 1, \
     "../data/frequencies/woman.csv" using 3:xticlabels(sprintf('%d', $1)) linestyle 3, \
     "../data/frequencies/woman.csv" using 4:xticlabels(sprintf('%d', $1)) linestyle 5, \
     "../data/frequencies/woman.csv" using 5:xticlabels(sprintf('%d', $1)) linestyle 7

set output "frequencies_bikes.pdf"
plot "../data/frequencies/bikes.csv" using 2:xticlabels(sprintf('%d', $1)) linestyle 1, \
     "../data/frequencies/bikes.csv" using 3:xticlabels(sprintf('%d', $1)) linestyle 3, \
     "../data/frequencies/bikes.csv" using 4:xticlabels(sprintf('%d', $1)) linestyle 5, \
     "../data/frequencies/bikes.csv" using 5:xticlabels(sprintf('%d', $1)) linestyle 7

set output "frequencies_lighthouse.pdf"
plot "../data/frequencies/lighthouse.csv" using 2:xticlabels(sprintf('%d', $1)) linestyle 1, \
     "../data/frequencies/lighthouse.csv" using 3:xticlabels(sprintf('%d', $1)) linestyle 3, \
     "../data/frequencies/lighthouse.csv" using 4:xticlabels(sprintf('%d', $1)) linestyle 5, \
     "../data/frequencies/lighthouse.csv" using 5:xticlabels(sprintf('%d', $1)) linestyle 7

set output "frequencies_parrots.pdf"
plot "../data/frequencies/parrots.csv" using 2:xticlabels(sprintf('%d', $1)) linestyle 1, \
     "../data/frequencies/parrots.csv" using 3:xticlabels(sprintf('%d', $1)) linestyle 3, \
     "../data/frequencies/parrots.csv" using 4:xticlabels(sprintf('%d', $1)) linestyle 5, \
     "../data/frequencies/parrots.csv" using 5:xticlabels(sprintf('%d', $1)) linestyle 7