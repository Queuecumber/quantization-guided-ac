#!/usr/bin/gnuplot -c
set terminal pdf enhanced color font "Helvetica, 30" size 9.5,3

set datafile separator ','

unset key
unset xtics
unset ytics

load 'plasma.pal'

set xrange [-23:23]
set yrange [-22:22]

set lmargin 0.45
set rmargin 0.5
set tmargin 0.4
set bmargin 0.4

set output "tsne.pdf"
set multiplot layout 1, 3 

unset colorbox

set size 0.3, 1
set origin 0, 0

plot '../data/manifold/cd_tsne.csv' using 2:3:1 with points pointtype 7 palette, \
     '../data/manifold/cd_tsne.csv' using 2:(stringcolumn(4) eq "True"?$3:1/0):1 with points pointtype 6 ps 1 lc 0 lw 2

set rmargin 0.1

set size 0.3, 1
set origin 0.294, 0

plot '../data/manifold/cec_tsne.csv' using 2:3:1 with points pointtype 7 palette, \
     '../data/manifold/cec_tsne.csv' using 2:(stringcolumn(4) eq "True"?$3:1/0):1 with points pointtype 6 ps 1 lc 0 lw 2
set rmargin 0.1

set size 0.38, 1
set origin 0.595, 0
set colorbox

plot '../data/manifold/cey_tsne.csv' using 2:3:1 with points pointtype 7 palette, \
     '../data/manifold/cey_tsne.csv' using 2:(stringcolumn(4) eq "True"?$3:1/0):1 with points pointtype 6 ps 1 lc 0 lw 2
