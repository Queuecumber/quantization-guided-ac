#!/usr/bin/gnuplot -c

set terminal pdf enhanced color font "Helvetica, 30" size 7, 6

set key autotitle columnhead 

unset key
unset colorbox

load 'plasma.pal'

set grid ytics xtics

set ylabel 'Increase in SSIM'
set xlabel 'Increase in PSNR (dB)'

set xtics 0.25
set ytics 0.01

load 'plasma.pal'

set datafile separator ','

set output "compare.pdf"

set cbrange [0:15]
plot "../data/comparisons/extended_live1_10.csv" every :::0::0 using 2:3:1:0 with labels left point linestyle 1 pointtype 7 pointsize 2 palette offset char 1, 0 font "Helvetica, 20", \
     "../data/comparisons/extended_live1_10.csv" every :::1::1 using 2:3:1:($0 + 8) with labels left point linestyle 1 pointtype 7 pointsize 2 palette offset char 1, -0.1 font "Helvetica, 20", \
     "../data/comparisons/extended_live1_10.csv" every :::2::2 using 2:3:1:($0 + 10) with labels left point linestyle 1 pointtype 7 pointsize 2 palette offset char 1, -0.2 font "Helvetica, 20", \
     "../data/comparisons/extended_live1_10.csv" every :::3::3 using 2:3:1:($0 + 12) with labels left point linestyle 1 pointtype 7 pointsize 2 palette offset char -1, -0.6 font "Helvetica, 20", \
     "../data/comparisons/extended_live1_10.csv" every :::4::4 using 2:3:1:($0 + 14) with labels left point linestyle 1 pointtype 7 pointsize 2 palette offset char 1, 0 font "Helvetica Bold, 20", \
     "../data/comparisons/extended_live1_10.csv" every :::4::4 using 2:3:1 with labels left point linestyle 1 pointtype 6 pointsize 2 linewidth 3 linecolor "black" offset char 1, 0 font "Helvetica Bold, 20", 


