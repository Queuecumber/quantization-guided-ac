\section{Results}

We validate the theoretical discussion in the previous sections with experimental results. We first describe the datasets
we used along with the training procedure we followed. We then show both grayscale and
color artifact correction results and compare with previous state-of-the-art methods. Finally we perform an ablation study. 
Please see our supplementary material for futher
results. In the following section we stress that the models we are comparing against train a bespoke model for each JPEG quality
setting they test on while we train only a single model.

\subsection{Datasets and Training Procedure}
For training we use the DIV2k and Flickr2k \cite{agustsson2017ntire} datasets. DIV2k consists of 800 
training and 100 validation images and the Flickr2k dataset contains 2650 images. We preextract $256 \times 256$ patches from these 
images taking 30 
random patches from each image and compress them using quality in $[10, 100]$ in steps of 10. This gives a total training set of 1,065,000 
patches. All training uses the Adam \cite{kingma2014adam} optimizer with a batch size of 32 patches. Our network is implemented using
the PyTorch \cite{NEURIPS2019_9015} library.

For evaluation we use the Live1 \cite{sheikh2006statistical, sheikh2006live}, Classic-5 
BSDS500 \cite{arbelaez2010contour}, and ICB datasets \cite{icb}. ICB is a new dataset which provides 15 high quality lossless images 
designed specifically to measure compression quality. It is our hope that the community will gradually begin including ICB dataset 
results. Where previous works have provided code and models, we reevaluate their methods and provide 
results here for comparison for comparison. As with all prior works we report PSNR, PSNR-B \cite{tadala2012novel}, and
SSIM \cite{wang2004image}.

We normalize the DCT coefficients using per-frequency and per-channel mean and standard deviations. Since the DCT coefficients 
are measurements of different signals, by computing the statistics per-frequency we normalize the distributions
so that they are all roughly the same magnitude. We find that this greatly speeds up the convergence of the 
network. Quantization table entrys are
normalized to [0, 1] with 1 being the most quantization and 0 the least. We use libjpeg \cite{libjpeg} for compression
and use the baseline quantization setting.

\subsection{Grayscale Artifact Correction}

For this 
task, the image is converted to YCbCr and only the Y channel is compressed and corrected. Our learning rate starts at 
$1 \times 10^{-3}$ and decays by a factor of 2 every 100,000 batches. We stop training after 400,000 batches (this is approximately
12 epochs of the 1,065,000 patch training set). We set $\lambda$ in Equation \ref{eq:regressionloss} to 0.05. We report 
results in Table \ref{tab:yresults} for Live1, Classic-5, BSDS500, and ICB. 
As the table shows, our model is able to match or outperform previous state-of-the-art models in many cases, and consistently provides
high SSIM results using one model for all quality factors. All the comparisons in Table \ref{tab:yresults} use models trained specifically
for the given quality factor. 

\begin{table}[t]
    \centering
    \resizebox{\columnwidth}{!}{
        \begin{tabular}{r *{7}{c}}
            \toprule
            Dataset & Quality & JPEG & ARCNN\cite{dong2015compression} &  MWCNN \cite{liu2018multi} & IDCN \cite{zheng2019implicit} & DMCNN \cite{zhang2018dmcnn} & Ours \\
            \midrule
            \multirow{3}{*}{Live-1} & \csvreader[late after line=\\]{data/comparisons/live1.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
            \midrule
            \multirow{3}{*}{Classic-5} & \csvreader[late after line=\\]{data/comparisons/classic5.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
            \midrule
            \multirow{3}{*}{BSDS500} & \csvreader[late after line=\\]{data/comparisons//BSDS500.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
            \midrule
            \multirow{3}{*}{ICB} & \csvreader[late after line=\\]{data/comparisons//ICB-GRAY8.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
            \bottomrule
        \end{tabular}
    }      
    \caption{Y channel Correction Results. Shown in PSNR / PSNR-B / SSIM format, the best result is highlighted in bold. The JPEG column gives
    with input error of the images. For ICB, we used the Grayscale 8bit dataset.}
    \label{tab:yresults}
\end{table}

\subsection{Color Artifact Correction}
\begin{table}[t]
    \centering
    \resizebox{\columnwidth}{!}{
        \begin{tabular}{r *{7}{c}}
            \toprule
            Dataset & Quality & JPEG & ARCNN\cite{dong2015compression} &  MWCNN \cite{liu2018multi} & IDCN \cite{zheng2019implicit} & DMCNN \cite{zhang2018dmcnn} & Ours \\
            \midrule
            \multirow{3}{*}{Live-1} & \csvreader[late after line=\\]{data/comparisons/live1_color.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
            \midrule
            \multirow{3}{*}{BSDS500} & \csvreader[late after line=\\]{data/comparisons/BSDS500_color.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
            \midrule
            \multirow{3}{*}{ICB} & \csvreader[late after line=\\]{data/comparisons/ICB-RGB8.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow} 
            \bottomrule
        \end{tabular}
    }
    \caption{Color Artifact Correction Results, PSNR / PSNR-B / SSIM format. Best result in bold, JPEG column gives input error. For ICB
    we used the RGB 8bit dataset.}
    \label{tab:colorresults}
\end{table}

For this task all color channels are restored. During training, we freeze the Y channel network
weights which that were trained for grayscale artifact correction and train only the color channel network weights. We use a cosine
annealing learning rate schedule \cite{loshchilov2016sgdr} decaying from $1 \times 10^{-3}$ to $1 \times 10^{-6}$ over 100,000
batches (approximately 3 epochs). We report Live1 BSDS500, and ICB results in Table \ref{tab:colorresults}. On this task our model consistently
outperforms comparisons on all datasets. Note that of the comparisons, only ours and IDCN \cite{zheng2019implicit} include native processing of color channels. For
the other models, we convert input images to YCbCr and process the channels independently. As in the previous section, all the comparison models trained a unique network for each JPEG quality factor. 
We show qualitative results in Figure \ref{fig:gan}. For comparisons to more methods see Figure \ref{fig:compare}. Since our model is not restricted
by which quality settings it can be run on, we show the increase in PSNR using our model for qualities 10-90 in Figure \ref{fig:ipsnr}.

\begin{figure}[t]
    \begin{minipage}{0.49\linewidth}
        \centering
        \includegraphics[width=\linewidth]{plots/ipsnr.pdf}
        \caption{Increase in PSNR on color datasets. For all three datasets we show the average improvement in PSNR values on qualities 10-90.
        Improvement drops off steeply at qualty 90.}
        \label{fig:ipsnr}
    \end{minipage}
    \hspace{0.01\linewidth}
    \begin{minipage}{0.49\linewidth}
        \centering
        \includegraphics[width=\linewidth]{plots/compare.pdf}
        \caption{Comparison for Live-1 quality 10. Where
        code was available we reevaluated, otherwise we used published numbers. If no color results were published we 
        used Y channel results.}
        \label{fig:compare}
    \end{minipage}
\end{figure}

\subsection{GAN Correction}
\label{sec:res:gan}

We now show qualitative GAN results. Our GAN model is trained for 100,000 iterations using cosine annealing \cite{loshchilov2016sgdr}
starting from $1 \times 10^{-4}$ and ending at $1 \times 10^{-6}$. The model weights are initialized to the pre-trained color regression model
weights. We set $\gamma$ and $\nu$ in Equation \ref{eq:ganloss} to $5 \times 10^{-3}$
and $1 \times 10^{-2}$ respecively. We train the generator and disciminator a single iteration each at a time. We use the discriminator network 
from ESRGAN \cite{wang2018esrgan} that we modified to operate on $256 \times 256$ patches. We use model interpolation \cite{wang2018esrgan}
and show qualitative results with $\alpha = 0.7$ in Figure \ref{fig:gan}. We provide additional qualitative results, including for
other settings of $\alpha$, in the supplementary material.

\begin{figure}[t]
    \centering
    \resizebox{\columnwidth}{!}{
    \begin{tabular}{ccccccc}
        Original & JPEG & IDCN & Ours & Ours GAN \\
        \includegraphics[width=0.2\linewidth]{figures/comparison/artificial/original.png} & 
        \includegraphics[width=0.2\linewidth]{figures/comparison/artificial/jpeg.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/artificial/idcn.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/artificial/ours.png} & 
        \includegraphics[width=0.2\linewidth]{figures/comparison/artificial/gan.png} \\
        \includegraphics[width=0.2\linewidth]{figures/comparison/nightshot/original.png} & 
        \includegraphics[width=0.2\linewidth]{figures/comparison/nightshot/jpeg.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/nightshot/idcn.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/nightshot/ours.png} & 
        \includegraphics[width=0.2\linewidth]{figures/comparison/nightshot/gan.png} \\
        \includegraphics[width=0.2\linewidth]{figures/comparison/lamb/original.jpg} & 
        \includegraphics[width=0.2\linewidth]{figures/comparison/lamb/jpeg.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/lamb/idcn.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/lamb/ours.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/lamb/gan.png} \\
        \includegraphics[width=0.2\linewidth]{figures/comparison/lighthouse/original.png} & 
        \includegraphics[width=0.2\linewidth]{figures/comparison/lighthouse/jpeg.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/lighthouse/idcn.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/lighthouse/ours.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/lighthouse/gan.png} \\
        \includegraphics[width=0.2\linewidth]{figures/comparison/flower/original.png} & 
        \includegraphics[width=0.2\linewidth]{figures/comparison/flower/jpeg.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/flower/idcn.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/flower/ours.png} & 
        \includegraphics[width=0.2\linewidth]{figures/comparison/flower/gan.png} \\
        \includegraphics[width=0.2\linewidth]{figures/comparison/womanhat/original.png} & 
        \includegraphics[width=0.2\linewidth]{figures/comparison/womanhat/jpeg.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/womanhat/idcn.png} &
        \includegraphics[width=0.2\linewidth]{figures/comparison/womanhat/ours.png} & 
        \includegraphics[width=0.2\linewidth]{figures/comparison/womanhat/gan.png} \\
    \end{tabular}}
    \caption{Qualitative Results. All images were compressed at Quality 10. Please zoom in to view details.}
    \label{fig:gan}
\end{figure}

\subsection{Understanding Convolutional Filter Manifolds}

\TODO{Put this back for now}

\begin{figure}
    \begin{minipage}{0.49\linewidth}
        \centering
        \begin{tabular}{ccc}
            \includegraphics[width=0.33\linewidth]{figures/weights/q=10c=0.png} & \includegraphics[width=0.33\linewidth]{figures/weights/q=10c=15.png} & \includegraphics[width=0.33\linewidth]{figures/weights/q=10c=30.png} \\
            \includegraphics[width=0.33\linewidth]{figures/weights/q=50c=0.png} & \includegraphics[width=0.33\linewidth]{figures/weights/q=50c=15.png} & \includegraphics[width=0.33\linewidth]{figures/weights/q=50c=30.png} \\
            \includegraphics[width=0.33\linewidth]{figures/weights/q=100c=0.png} & \includegraphics[width=0.33\linewidth]{figures/weights/q=100c=15.png} & \includegraphics[width=0.33\linewidth]{figures/weights/q=100c=30.png}
        \end{tabular}
        \caption{CFM weight visualization. Horizontal axis shows different channels of the weight, vertical
        axis shows CFM weight visualization. Quality levels shown are Top: 10, Middle: 50, Bottom: 100.}
        \label{fig:weight_vis}
    \end{minipage}
    \hspace{0.01\linewidth}
    \begin{minipage}{0.49\linewidth}
        \centering
        \begin{tabular}{ccc}
            \includegraphics[width=0.33\linewidth]{figures/activations/q=10c=0.png} & \includegraphics[width=0.33\linewidth]{figures/activations/q=10c=15.png} & \includegraphics[width=0.33\linewidth]{figures/activations/q=10c=30.png} \\
            \includegraphics[width=0.33\linewidth]{figures/activations/q=50c=0.png} & \includegraphics[width=0.33\linewidth]{figures/activations/q=50c=15.png} & \includegraphics[width=0.33\linewidth]{figures/activations/q=50c=30.png} \\
            \includegraphics[width=0.33\linewidth]{figures/activations/q=100c=0.png} & \includegraphics[width=0.33\linewidth]{figures/activations/q=100c=15.png} & \includegraphics[width=0.33\linewidth]{figures/activations/q=100c=30.png}
        \end{tabular}
        \caption{Images which maximally activate CFM weights. Horizontal axis shows
        different channels from the weight, vertical axis shows quality. Quality levels shown are Top: 10, Middle: 50, Bottom: 100.}
        \label{fig:max_activation}
    \end{minipage}
\end{figure}

\begin{figure}[t]
    \centering
    \includegraphics[width=\linewidth]{plots/tsne.pdf}   
    \caption{Embeddings for different CFM layers. 3 channels are taken from each embedding, color shows JPEG quality
    setting that produced the input quantization matrix. Circled
    points indicate quantization matrices that were seen during training.}
    \label{fig:quality_tsne}
\end{figure}

CFM layers are both our largest departure
from a vanilla CNN and also quite important to learning quality invariant features, so it is a natural result to try to visualize 
their operation.  In Figure \ref{fig:weight_vis}, we 
compute the final $8 \times 8$ convolution weight for different quality levels. The quality levels, on the vertical axis, are 
10, 50, and 100. The horizontal axis shows three different channels from the weight. What we see makes intuitive sense: 
the filters in different channels have different patterns, but for the same channel, the pattern is roughly the same as the 
quality increases. Furthermore, the filter response becomes smaller as the quality increases since the filters have
to do less ``work'' to correct a high quality JPEG. 

Next we visualize compression artifacts learned by the weight. To do this we find the image that maximally activates a single channel of the
CFM weight. The result
of this is shown in Figure \ref{fig:max_activation}. Again the horizontal axis shows different channels of the weight and the vertical axis 
shows quality levels 10, 50, and 100. The result shows clear images of 
JPEG artifacts. At quality 10, the local blocking artifacts are extremely prominant. By 
quality 50, the blocking artifacts are suppressed, while structural artifacts remain. The qualtiy 100 images are almost untouched, leaving only the
input noise pattern. It makes sense that 
quality 100 filters are only minmally activated since there is not much correction to do on a quality 100 JPEG. Note that we only show Y
channel response for this figure and that Figures \ref{fig:weight_vis} and \ref{fig:max_activation} use the same channels from the same 
layer.

Finally we examine the manifold structure of the CFM. We claim in Section 3.1 (and the name implies) that the CFM learns a smooth manifold
of filters through quantization space. If this is true, then a quality 25 quantization matrix should generate a weight halfway inbetween
a qualty 20 and a quality 30 one. To show that this happens, we generate weights
for all 101 quanitzation matrices (0 to 100 inclusive) and then compute t-SNE 
embeddings to reduce the dimensionality to 2. We plot 3 channels from the weight embeddings with the quality level that was used to
generate the weight given 
as the color of the point. This plot is shown in Figure \ref{fig:quality_tsne}. What see is a smooth line through the space starting from 
dark (low quality) to bright (high quality) showing that the CFM has not only separated the different quality levels but has 
ordered them as well.
Futhermore we see that the low quality filters are separated in space, indicating that they are quite different (and
perform different functions), a property that is important for effective neural networks. As the quality increases and the problem becomes
easier, the filters tend to converge on a single point where they are all doing very little to correct the image.

\subsection{Generalization Comparisons}

As we have stressed repeatedly, a major advantage of our method is that it uses a single model to correct JPEG images at any quality, while
prior works train a model for each quality factor. Here, we explore if other methods are capable of generalizing or if they really require
this ensamble of models. To explore this, we use our closest competitor and prior state-of-the-art, IDCN \cite{zheng2019implicit}. IDCN
does not provide a model for quality higher than 20, so we explore if their model generalizes by using their quality 10 and quality 20 models to correct
quality 50 Live-1 images. We also test using the quality 20 model to correct quality 10 images and using the quality
10 model to correct quality 20 images. These results are shown in Table \ref{tab:genresults} along with our result for comparison. As the
table shows, the choice of model is critical and there is a signficant penalty for choosing the wrong model. Neither their quality 10 nor
their quality 20 model is able to effecively correct images that it was not trained on, scoring significantly lower than if the 
correct model were used. At quality 50, the quality 10 model produces a result worse than the input JPEG, and the quality 20 model makes
only a slight improvement. We stress that the quality setting is not stored in the JPEG file, so a deployed system has no way to pick the
correct model. Since our model adapts to the quantization matrix, which is stored with the JPEG file, this limitation is aleviated. We show
an example of a quality 50 image in Figure \ref{fig:gen}.

\begin{table}
    \centering
    \resizebox{\columnwidth}{!}{
        \begin{tabular}{*{5}{c}}
            \toprule
            Model Quality & Image Quality & JPEG & IDCN \cite{zheng2019implicit} & Ours \\
            \midrule
            \csvreader[late after line=\\]{data/generalization/live1_generalize.csv}{}{\csvlinetotablerow} 
            \bottomrule
        \end{tabular}
    }
    \caption{IDCN generalization on Live-1}
    \label{tab:genresults}
\end{table}

\begin{figure}
    \centering
    \resizebox{\columnwidth}{!}{
    \begin{tabular}{ccccc}
        Original & JPEG & IDCN Q=10 & IDCN Q=20 & Ours \\
        \includegraphics[width=0.2\linewidth]{figures/generalize/original.png} & 
        \includegraphics[width=0.2\linewidth]{figures/generalize/jpeg.png} &
        \includegraphics[width=0.2\linewidth]{figures/generalize/idcn_10.png} &
        \includegraphics[width=0.2\linewidth]{figures/generalize/idcn_20.png} & 
        \includegraphics[width=0.2\linewidth]{figures/generalize/ours.png} 
    \end{tabular}}
    \caption{Generalization example for a quality 50 input. Please zoom in to view details.}
    \label{fig:gen}
\end{figure}


\subsection{Ablation}
\label{sec:res:ablation}

In this section
we ablate many of our design decisions and observe their effect on the network accuracy. For all tests, we keep the number of parameters 
approximately 
the same between tested models to control
for one network performing better simply because it has higher capacity. The results are in Table \ref{tab:ablation}, we report
all three metrics on quality 10 classic-5. All models are trained for 100,000 batches on the grayscale training patch set using cosine
annealing \cite{loshchilov2016sgdr} from a learning rate of $1 \times 10^{-3}$ to $1 \times 10^{-6}$.

Our discussion placed great empahsis on the adaptable weights in the CFM layers, but there are other
simpler methods of using side channel information. We could simply concatenate the quantization matrix channelwise with the input, or we
could ignore the quantization matrix altogether. As shown in the ``CFM'' experiment the CFM unit performs better than both of these alternatives by 
a considerable margin.

In our discussion, we 
noted that the frequencynet should not be able
to perform without a preceeding blocknet because high frequency information will be zeroed out from the compression process. As shown in 
the ``Block vs Frequency'' experiment, the Blocknet alone attains significantly higher performance than the frequencynet alone.

In the final experiment, titled ``Fusion'', we test the necessity of the fusion layer. We claimed that this fusion layer
was necessary for gradient flow to the early layers of our network. The network without fusion fails to learn, matching the input PSNR of 
classic-5 after full training, whereas the network with fusion makes considerable progress.


\begin{table}
    \centering
    \begin{tabular}{r *{7}{c}}
        \toprule
        Experiment & Model & PSNR & PSNR-B & SSIM \\
        \midrule
        \multirow{3}{*}{CFM} & \csvreader[late after line=\\]{data/ablation/cmf.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
        \midrule
        \multirow{2}{*}{Block vs Frequency} & \csvreader[late after line=\\]{data/ablation/bvf.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
        \midrule
        \multirow{2}{*}{Fusion} & \csvreader[late after line=\\]{data/ablation/ft.csv}{}{\csviffirstrow{}{&}\csvlinetotablerow}
        \bottomrule
    \end{tabular}    
    \caption{Ablation results}
    \label{tab:ablation}
\end{table}
